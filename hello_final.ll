; ModuleID = 'hello_final.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32-f128:128:128-n8:16:32"
target triple = "i386-pc-linux-gnu"

%0 = type { i32, void ()* }
%struct..1__pthread_mutex_s = type { i32, i32, i32, i32, i32, %union..0._10 }
%"struct.AVLNode<MemoryBlock,i8*>" = type { %struct.MemoryBlock, %"struct.AVLNode<MemoryBlock,i8*>"*, %"struct.AVLNode<MemoryBlock,i8*>"*, %"struct.AVLNode<MemoryBlock,i8*>"*, i32 }
%"struct.AVLTree<MemoryBlock,i8*>" = type { %"struct.AVLNode<MemoryBlock,i8*>"*, i32 }
%struct.MemoryBlock = type { i8*, i32, i8, i8 }
%struct.__pthread_slist_t = type { %struct.__pthread_slist_t* }
%"struct.std::basic_ios<char,std::char_traits<char> >" = type { %"struct.std::ios_base", %"struct.std::ostream"*, i8, i8, %"struct.std::basic_streambuf<char,std::char_traits<char> >"*, %"struct.std::ctype<char>"*, %"struct.std::num_get<char,std::istreambuf_iterator<char, std::char_traits<char> > >"*, %"struct.std::num_get<char,std::istreambuf_iterator<char, std::char_traits<char> > >"* }
%"struct.std::basic_streambuf<char,std::char_traits<char> >" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"struct.std::locale" }
%"struct.std::ctype<char>" = type { %"struct.std::locale::facet", i32*, i8, i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8 }
%"struct.std::ios_base" = type { i32 (...)**, i32, i32, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"struct.std::locale" }
%"struct.std::ios_base::Init" = type <{ i8 }>
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"struct.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type { i8*, i32 }
%"struct.std::locale" = type { %"struct.std::locale::_Impl"* }
%"struct.std::locale::_Impl" = type { i32, %"struct.std::locale::facet"**, i32, %"struct.std::locale::facet"**, i8** }
%"struct.std::locale::facet" = type { i32 (...)**, i32 }
%"struct.std::num_get<char,std::istreambuf_iterator<char, std::char_traits<char> > >" = type { %"struct.std::locale::facet" }
%"struct.std::num_put<char,std::ostreambuf_iterator<char, std::char_traits<char> > >" = type { %"struct.std::locale::facet" }
%"struct.std::ostream" = type { i32 (...)**, %"struct.std::basic_ios<char,std::char_traits<char> >" }
%union..0._10 = type { i32 }
%union.pthread_attr_t = type { i32, [8 x i32] }
%union.pthread_mutex_t = type { %struct..1__pthread_mutex_s }
%union.pthread_mutexattr_t = type { i32 }

@ptr_int1 = internal global i32* null
@_ZStL8__ioinit = internal global %"struct.std::ios_base::Init" zeroinitializer
@__dso_handle = external global i8*
@_ZL6mbTree = internal global %"struct.AVLTree<MemoryBlock,i8*>" zeroinitializer
@.str = private constant [6 x i8] c"freed\00", align 1
@.str1 = private constant [10 x i8] c"not freed\00", align 1
@.str2 = private constant [9 x i8] c"accessed\00", align 1
@.str3 = private constant [13 x i8] c"not accessed\00", align 1
@.str4 = private constant [2 x i8] c"[\00", align 1
@.str5 = private constant [2 x i8] c",\00", align 1
@.str6 = private constant [3 x i8] c", \00", align 1
@.str7 = private constant [2 x i8] c"]\00", align 1
@.str8 = private constant [6 x i8] c" bf: \00", align 1
@.str9 = private constant [26 x i8] c"AVL Tree print(inorder): \00", align 1
@.str10 = private constant [19 x i8] c"Number of Nodes : \00", align 1
@.str11 = private constant [19 x i8] c"memory block list:\00", align 1
@_ZSt4cout = external global %"struct.std::ostream"
@.str12 = private constant [44 x i8] c"At line %d: accesse memory block [%u,%u]  \0A\00", align 4
@_ZSt4cerr = external global %"struct.std::ostream"
@.str14 = private constant [31 x i8] c"new AVLNode failed in insert()\00", align 4
@.str15 = private constant [59 x i8] c"At line %d:  %d=malloc(%d),instrument mbSetMalloc (%d,%d)\0A\00", align 4
@llvm.global_ctors = appending global [1 x %0] [%0 { i32 65535, void ()* @_GLOBAL__I_mbSetMalloc }]

@_ZL20__gthrw_pthread_oncePiPFvvE = alias weak i32 (i32*, void ()*)* @pthread_once
@_ZL27__gthrw_pthread_getspecificj = alias weak i8* (i32)* @pthread_getspecific
@_ZL27__gthrw_pthread_setspecificjPKv = alias weak i32 (i32, i8*)* @pthread_setspecific
@_ZL22__gthrw_pthread_createPmPK14pthread_attr_tPFPvS3_ES3_ = alias weak i32 (i32*, %union.pthread_attr_t*, i8* (i8*)*, i8*)* @pthread_create
@_ZL22__gthrw_pthread_cancelm = alias weak i32 (i32)* @pthread_cancel
@_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t = alias weak i32 (%union.pthread_mutex_t*)* @pthread_mutex_lock
@_ZL29__gthrw_pthread_mutex_trylockP15pthread_mutex_t = alias weak i32 (%union.pthread_mutex_t*)* @pthread_mutex_trylock
@_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t = alias weak i32 (%union.pthread_mutex_t*)* @pthread_mutex_unlock
@_ZL26__gthrw_pthread_mutex_initP15pthread_mutex_tPK19pthread_mutexattr_t = alias weak i32 (%union.pthread_mutex_t*, %union..0._10*)* @pthread_mutex_init
@_ZL26__gthrw_pthread_key_createPjPFvPvE = alias weak i32 (i32*, void (i8*)*)* @pthread_key_create
@_ZL26__gthrw_pthread_key_deletej = alias weak i32 (i32)* @pthread_key_delete
@_ZL30__gthrw_pthread_mutexattr_initP19pthread_mutexattr_t = alias weak i32 (%union..0._10*)* @pthread_mutexattr_init
@_ZL33__gthrw_pthread_mutexattr_settypeP19pthread_mutexattr_ti = alias weak i32 (%union..0._10*, i32)* @pthread_mutexattr_settype
@_ZL33__gthrw_pthread_mutexattr_destroyP19pthread_mutexattr_t = alias weak i32 (%union..0._10*)* @pthread_mutexattr_destroy

define i32 @main() nounwind {
entry:
  %accessedAddress_addr.i34 = alloca i8*, align 4
  %accessedAddress_addr.i31 = alloca i8*, align 4
  %accessedAddress_addr.i28 = alloca i8*, align 4
  %accessedAddress_addr.i25 = alloca i8*, align 4
  %accessedAddress_addr.i22 = alloca i8*, align 4
  %accessedAddress_addr.i19 = alloca i8*, align 4
  %accessedAddress_addr.i16 = alloca i8*, align 4
  %accessedAddress_addr.i13 = alloca i8*, align 4
  %accessedAddress_addr.i10 = alloca i8*, align 4
  %accessedAddress_addr.i7 = alloca i8*, align 4
  %accessedAddress_addr.i4 = alloca i8*, align 4
  %accessedAddress_addr.i1 = alloca i8*, align 4
  %accessedAddress_addr.i = alloca i8*, align 4
  %retval = alloca i32, align 4
  %0 = alloca i32, align 4
  %i = alloca i32, align 4
  call void @llvm.dbg.declare(metadata !8, metadata !9), !dbg !11
  %1 = bitcast i32* %i to i8*
  store i8* %1, i8** %accessedAddress_addr.i, align 4
  %.val48 = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  %2 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val48, i8** %accessedAddress_addr.i) nounwind
  %3 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %2, null
  br i1 %3, label %mbSetAccessed.exit, label %bb.i

bb.i:                                             ; preds = %entry
  %4 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %2, i32 0, i32 0, i32 2
  store i8 1, i8* %4, align 4
  %5 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %2, i32 0, i32 0, i32 1
  %6 = load i32* %5, align 4
  %7 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %2, i32 0, i32 0, i32 0
  %8 = load i8** %7, align 4
  %9 = ptrtoint i8* %8 to i32
  %10 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 6, i32 %9, i32 %6) nounwind
  br label %mbSetAccessed.exit

mbSetAccessed.exit:                               ; preds = %bb.i, %entry
  store i32 0, i32* %i, align 4, !dbg !11
  br label %bb1, !dbg !11

bb:                                               ; preds = %mbSetAccessed.exit24
  store i8* %1, i8** %accessedAddress_addr.i1, align 4
  %.val47 = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  %11 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val47, i8** %accessedAddress_addr.i1) nounwind
  %12 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %11, null
  br i1 %12, label %mbSetAccessed.exit3, label %bb.i2

bb.i2:                                            ; preds = %bb
  %13 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %11, i32 0, i32 0, i32 2
  store i8 1, i8* %13, align 4
  %14 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %11, i32 0, i32 0, i32 1
  %15 = load i32* %14, align 4
  %16 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %11, i32 0, i32 0, i32 0
  %17 = load i8** %16, align 4
  %18 = ptrtoint i8* %17 to i32
  %19 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 8, i32 %18, i32 %15) nounwind
  %.pre = load i32* %i, align 4
  br label %mbSetAccessed.exit3

mbSetAccessed.exit3:                              ; preds = %bb.i2, %bb
  %20 = phi i32 [ %225, %bb ], [ %.pre, %bb.i2 ]
  %tmp = mul i32 %20, 10
  %21 = add i32 %tmp, 10, !dbg !12
  %22 = call noalias i8* @malloc(i32 %21) nounwind, !dbg !12
  %23 = ptrtoint i8* %22 to i32
  %24 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([59 x i8]* @.str15, i32 0, i32 0), i32 8, i32 %23, i32 %21, i32 %23, i32 %21) nounwind
  %25 = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  br label %bb6.i.i

bb.i1.i:                                          ; preds = %bb6.i.i
  %26 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.0.i.i, i32 0, i32 0, i32 0
  %27 = load i8** %26, align 4
  %28 = icmp eq i8* %22, %27
  br i1 %28, label %bb.i.i.i, label %bb2.i.i

bb.i.i.i:                                         ; preds = %bb.i1.i
  %29 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.0.i.i, i32 0, i32 0, i32 1
  %30 = load i32* %29, align 4
  %31 = icmp eq i32 %21, %30
  br i1 %31, label %mbSetMalloc.exit, label %bb2.i.i

bb2.i.i:                                          ; preds = %bb.i.i.i, %bb.i1.i
  %32 = icmp ult i8* %22, %27
  br i1 %32, label %bb4.i.i, label %bb1.i11.i.i

bb1.i11.i.i:                                      ; preds = %bb2.i.i
  br i1 %28, label %bb2.i12.i.i, label %bb5.i.i

bb2.i12.i.i:                                      ; preds = %bb1.i11.i.i
  %33 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.0.i.i, i32 0, i32 0, i32 1
  %34 = load i32* %33, align 4
  %35 = icmp slt i32 %21, %34
  br i1 %35, label %bb4.i.i, label %bb5.i.i

bb4.i.i:                                          ; preds = %bb2.i12.i.i, %bb2.i.i
  %36 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.0.i.i, i32 0, i32 1
  br label %bb6.i.i.backedge

bb6.i.i.backedge:                                 ; preds = %bb5.i.i, %bb4.i.i
  %p.0.i.i.be.in = phi %"struct.AVLNode<MemoryBlock,i8*>"** [ %37, %bb5.i.i ], [ %36, %bb4.i.i ]
  %p.0.i.i.be = load %"struct.AVLNode<MemoryBlock,i8*>"** %p.0.i.i.be.in, align 4
  br label %bb6.i.i

bb5.i.i:                                          ; preds = %bb2.i12.i.i, %bb1.i11.i.i
  %37 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.0.i.i, i32 0, i32 2
  br label %bb6.i.i.backedge

bb6.i.i:                                          ; preds = %bb6.i.i.backedge, %mbSetAccessed.exit3
  %parent.i.i.0 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ null, %mbSetAccessed.exit3 ], [ %p.0.i.i, %bb6.i.i.backedge ]
  %p.0.i.i = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %25, %mbSetAccessed.exit3 ], [ %p.0.i.i.be, %bb6.i.i.backedge ]
  %38 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %p.0.i.i, null
  br i1 %38, label %bb7.i.i, label %bb.i1.i

bb7.i.i:                                          ; preds = %bb6.i.i
  %39 = call i8* @_Znwj(i32 28) nounwind
  %40 = bitcast i8* %39 to %"struct.AVLNode<MemoryBlock,i8*>"*
  %41 = bitcast i8* %39 to i8**
  store i8* %22, i8** %41, align 4
  %42 = getelementptr inbounds i8* %39, i32 4
  %43 = bitcast i8* %42 to i32*
  store i32 %21, i32* %43, align 4
  %44 = getelementptr inbounds i8* %39, i32 8
  store i8 0, i8* %44, align 4
  %45 = getelementptr inbounds i8* %39, i32 9
  store i8 0, i8* %45, align 1
  %46 = getelementptr inbounds i8* %39, i32 12
  %47 = bitcast i8* %46 to %"struct.AVLNode<MemoryBlock,i8*>"**
  store %"struct.AVLNode<MemoryBlock,i8*>"* null, %"struct.AVLNode<MemoryBlock,i8*>"** %47, align 4
  %48 = getelementptr inbounds i8* %39, i32 16
  %49 = bitcast i8* %48 to %"struct.AVLNode<MemoryBlock,i8*>"**
  store %"struct.AVLNode<MemoryBlock,i8*>"* null, %"struct.AVLNode<MemoryBlock,i8*>"** %49, align 4
  %50 = getelementptr inbounds i8* %39, i32 20
  %51 = bitcast i8* %50 to %"struct.AVLNode<MemoryBlock,i8*>"**
  store %"struct.AVLNode<MemoryBlock,i8*>"* null, %"struct.AVLNode<MemoryBlock,i8*>"** %51, align 4
  %52 = getelementptr inbounds i8* %39, i32 24
  %53 = bitcast i8* %52 to i32*
  store i32 0, i32* %53, align 4
  %54 = icmp eq i8* %39, null
  br i1 %54, label %bb9.i.i, label %bb10.i.i

bb9.i.i:                                          ; preds = %bb7.i.i
  %55 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* @_ZSt4cerr, i8* getelementptr inbounds ([31 x i8]* @.str14, i32 0, i32 0)) nounwind
  %56 = call %"struct.std::ostream"* @_ZNSolsEPFRSoS_E(%"struct.std::ostream"* %55, %"struct.std::ostream"* (%"struct.std::ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  call void @exit(i32 -1) noreturn nounwind
  unreachable

bb10.i.i:                                         ; preds = %bb7.i.i
  %57 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %parent.i.i.0, null
  br i1 %57, label %bb11.i.i, label %bb12.i.i

bb11.i.i:                                         ; preds = %bb10.i.i
  store %"struct.AVLNode<MemoryBlock,i8*>"* %40, %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  br label %bb.i.i

bb12.i.i:                                         ; preds = %bb10.i.i
  store %"struct.AVLNode<MemoryBlock,i8*>"* %parent.i.i.0, %"struct.AVLNode<MemoryBlock,i8*>"** %51, align 4
  %58 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %parent.i.i.0, i32 0, i32 0, i32 0
  %59 = load i8** %58, align 4
  %60 = icmp ult i8* %22, %59
  br i1 %60, label %bb14.i.i, label %bb1.i27.i.i

bb1.i27.i.i:                                      ; preds = %bb12.i.i
  %61 = icmp eq i8* %22, %59
  br i1 %61, label %bb2.i28.i.i, label %bb15.i.i

bb2.i28.i.i:                                      ; preds = %bb1.i27.i.i
  %62 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %parent.i.i.0, i32 0, i32 0, i32 1
  %63 = load i32* %62, align 4
  %64 = icmp slt i32 %21, %63
  br i1 %64, label %bb14.i.i, label %bb15.i.i

bb14.i.i:                                         ; preds = %bb2.i28.i.i, %bb12.i.i
  %65 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %parent.i.i.0, i32 0, i32 1
  store %"struct.AVLNode<MemoryBlock,i8*>"* %40, %"struct.AVLNode<MemoryBlock,i8*>"** %65, align 4
  br label %bb33.i.i

bb15.i.i:                                         ; preds = %bb2.i28.i.i, %bb1.i27.i.i
  %66 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %parent.i.i.0, i32 0, i32 2
  store %"struct.AVLNode<MemoryBlock,i8*>"* %40, %"struct.AVLNode<MemoryBlock,i8*>"** %66, align 4
  br label %bb33.i.i

bb17.i.i:                                         ; preds = %bb33.i.i
  %67 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %136, i32 0, i32 1
  %68 = load %"struct.AVLNode<MemoryBlock,i8*>"** %67, align 4
  %69 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %68, %p.1.i.i
  %70 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %136, i32 0, i32 4
  %71 = load i32* %70, align 4
  %storemerge.v.i.i = select i1 %69, i32 -1, i32 1
  %storemerge.i.i = add i32 %71, %storemerge.v.i.i
  store i32 %storemerge.i.i, i32* %70, align 4
  switch i32 %storemerge.i.i, label %bb24.i.i [
    i32 0, label %bb34.i.i
    i32 1, label %bb33.i.i
    i32 -1, label %bb33.i.i
  ]

bb24.i.i:                                         ; preds = %bb17.i.i
  %72 = icmp slt i32 %storemerge.i.i, 0
  %73 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.1.i.i, i32 0, i32 4
  %74 = load i32* %73, align 4
  br i1 %72, label %bb25.i.i, label %bb29.i.i

bb25.i.i:                                         ; preds = %bb24.i.i
  %75 = icmp slt i32 %74, 0
  %76 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %68, i32 0, i32 2
  %77 = load %"struct.AVLNode<MemoryBlock,i8*>"** %76, align 4
  br i1 %75, label %bb26.i.i, label %bb27.i.i

bb26.i.i:                                         ; preds = %bb25.i.i
  store %"struct.AVLNode<MemoryBlock,i8*>"* %77, %"struct.AVLNode<MemoryBlock,i8*>"** %67, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %136, %"struct.AVLNode<MemoryBlock,i8*>"** %76, align 4
  %78 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %136, i32 0, i32 3
  %79 = load %"struct.AVLNode<MemoryBlock,i8*>"** %78, align 4
  %80 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %68, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %79, %"struct.AVLNode<MemoryBlock,i8*>"** %80, align 4
  %81 = load %"struct.AVLNode<MemoryBlock,i8*>"** %67, align 4
  %82 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %81, null
  br i1 %82, label %_ZN7AVLTreeI11MemoryBlockPcE7rotateRERP7AVLNodeIS0_S1_E.exit.i.i, label %bb.i21.i.i

bb.i21.i.i:                                       ; preds = %bb26.i.i
  %83 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %81, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %136, %"struct.AVLNode<MemoryBlock,i8*>"** %83, align 4
  br label %_ZN7AVLTreeI11MemoryBlockPcE7rotateRERP7AVLNodeIS0_S1_E.exit.i.i

_ZN7AVLTreeI11MemoryBlockPcE7rotateRERP7AVLNodeIS0_S1_E.exit.i.i: ; preds = %bb.i21.i.i, %bb26.i.i
  store %"struct.AVLNode<MemoryBlock,i8*>"* %68, %"struct.AVLNode<MemoryBlock,i8*>"** %78, align 4
  store i32 0, i32* %70, align 4
  %84 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %68, i32 0, i32 4
  store i32 0, i32* %84, align 4
  br label %bb34.i.i

bb27.i.i:                                         ; preds = %bb25.i.i
  %85 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %77, i32 0, i32 1
  %86 = load %"struct.AVLNode<MemoryBlock,i8*>"** %85, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %86, %"struct.AVLNode<MemoryBlock,i8*>"** %76, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %68, %"struct.AVLNode<MemoryBlock,i8*>"** %85, align 4
  %87 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %77, i32 0, i32 4
  %88 = load i32* %87, align 4
  %89 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %68, i32 0, i32 4
  %not..i.i.i = icmp sgt i32 %88, 0
  %..i.i.i = sext i1 %not..i.i.i to i32
  store i32 %..i.i.i, i32* %89, align 4
  %90 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %77, i32 0, i32 2
  %91 = load %"struct.AVLNode<MemoryBlock,i8*>"** %90, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %91, %"struct.AVLNode<MemoryBlock,i8*>"** %67, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %136, %"struct.AVLNode<MemoryBlock,i8*>"** %90, align 4
  %92 = load i32* %87, align 4
  %93 = icmp eq i32 %92, -1
  %storemerge1.i.i.i = zext i1 %93 to i32
  store i32 %storemerge1.i.i.i, i32* %70, align 4
  store i32 0, i32* %87, align 4
  %94 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %136, i32 0, i32 3
  %95 = load %"struct.AVLNode<MemoryBlock,i8*>"** %94, align 4
  %96 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %77, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %95, %"struct.AVLNode<MemoryBlock,i8*>"** %96, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %77, %"struct.AVLNode<MemoryBlock,i8*>"** %94, align 4
  %97 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %68, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %77, %"struct.AVLNode<MemoryBlock,i8*>"** %97, align 4
  %98 = load %"struct.AVLNode<MemoryBlock,i8*>"** %76, align 4
  %99 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %98, null
  br i1 %99, label %bb7.i.i.i, label %bb6.i.i.i

bb6.i.i.i:                                        ; preds = %bb27.i.i
  %100 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %98, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %68, %"struct.AVLNode<MemoryBlock,i8*>"** %100, align 4
  br label %bb7.i.i.i

bb7.i.i.i:                                        ; preds = %bb6.i.i.i, %bb27.i.i
  %101 = load %"struct.AVLNode<MemoryBlock,i8*>"** %67, align 4
  %102 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %101, null
  br i1 %102, label %bb34.i.i, label %bb8.i.i.i

bb8.i.i.i:                                        ; preds = %bb7.i.i.i
  %103 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %101, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %136, %"struct.AVLNode<MemoryBlock,i8*>"** %103, align 4
  br label %bb34.i.i

bb29.i.i:                                         ; preds = %bb24.i.i
  %104 = icmp sgt i32 %74, 0
  %105 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %136, i32 0, i32 2
  %106 = load %"struct.AVLNode<MemoryBlock,i8*>"** %105, align 4
  %107 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %106, i32 0, i32 1
  %108 = load %"struct.AVLNode<MemoryBlock,i8*>"** %107, align 4
  br i1 %104, label %bb30.i.i, label %bb31.i.i

bb30.i.i:                                         ; preds = %bb29.i.i
  store %"struct.AVLNode<MemoryBlock,i8*>"* %108, %"struct.AVLNode<MemoryBlock,i8*>"** %105, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %136, %"struct.AVLNode<MemoryBlock,i8*>"** %107, align 4
  %109 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %136, i32 0, i32 3
  %110 = load %"struct.AVLNode<MemoryBlock,i8*>"** %109, align 4
  %111 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %106, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %110, %"struct.AVLNode<MemoryBlock,i8*>"** %111, align 4
  %112 = load %"struct.AVLNode<MemoryBlock,i8*>"** %105, align 4
  %113 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %112, null
  br i1 %113, label %_ZN7AVLTreeI11MemoryBlockPcE7rotateLERP7AVLNodeIS0_S1_E.exit.i.i, label %bb.i17.i.i

bb.i17.i.i:                                       ; preds = %bb30.i.i
  %114 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %112, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %136, %"struct.AVLNode<MemoryBlock,i8*>"** %114, align 4
  br label %_ZN7AVLTreeI11MemoryBlockPcE7rotateLERP7AVLNodeIS0_S1_E.exit.i.i

_ZN7AVLTreeI11MemoryBlockPcE7rotateLERP7AVLNodeIS0_S1_E.exit.i.i: ; preds = %bb.i17.i.i, %bb30.i.i
  store %"struct.AVLNode<MemoryBlock,i8*>"* %106, %"struct.AVLNode<MemoryBlock,i8*>"** %109, align 4
  store i32 0, i32* %70, align 4
  %115 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %106, i32 0, i32 4
  store i32 0, i32* %115, align 4
  br label %bb34.i.i

bb31.i.i:                                         ; preds = %bb29.i.i
  %116 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %108, i32 0, i32 2
  %117 = load %"struct.AVLNode<MemoryBlock,i8*>"** %116, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %117, %"struct.AVLNode<MemoryBlock,i8*>"** %107, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %106, %"struct.AVLNode<MemoryBlock,i8*>"** %116, align 4
  %118 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %108, i32 0, i32 4
  %119 = load i32* %118, align 4
  %120 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %106, i32 0, i32 4
  %.lobit.i.i.i = lshr i32 %119, 31
  store i32 %.lobit.i.i.i, i32* %120, align 4
  %121 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %108, i32 0, i32 1
  %122 = load %"struct.AVLNode<MemoryBlock,i8*>"** %121, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %122, %"struct.AVLNode<MemoryBlock,i8*>"** %105, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %136, %"struct.AVLNode<MemoryBlock,i8*>"** %121, align 4
  %123 = load i32* %118, align 4
  %124 = icmp eq i32 %123, 1
  %storemerge1.i1.i.i = sext i1 %124 to i32
  store i32 %storemerge1.i1.i.i, i32* %70, align 4
  store i32 0, i32* %118, align 4
  %125 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %136, i32 0, i32 3
  %126 = load %"struct.AVLNode<MemoryBlock,i8*>"** %125, align 4
  %127 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %108, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %126, %"struct.AVLNode<MemoryBlock,i8*>"** %127, align 4
  %128 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %106, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %108, %"struct.AVLNode<MemoryBlock,i8*>"** %128, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %108, %"struct.AVLNode<MemoryBlock,i8*>"** %125, align 4
  %129 = load %"struct.AVLNode<MemoryBlock,i8*>"** %105, align 4
  %130 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %129, null
  br i1 %130, label %bb7.i3.i.i, label %bb6.i2.i.i

bb6.i2.i.i:                                       ; preds = %bb31.i.i
  %131 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %129, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %136, %"struct.AVLNode<MemoryBlock,i8*>"** %131, align 4
  br label %bb7.i3.i.i

bb7.i3.i.i:                                       ; preds = %bb6.i2.i.i, %bb31.i.i
  %132 = load %"struct.AVLNode<MemoryBlock,i8*>"** %107, align 4
  %133 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %132, null
  br i1 %133, label %bb34.i.i, label %bb8.i4.i.i

bb8.i4.i.i:                                       ; preds = %bb7.i3.i.i
  %134 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %132, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %106, %"struct.AVLNode<MemoryBlock,i8*>"** %134, align 4
  br label %bb34.i.i

bb33.i.i:                                         ; preds = %bb17.i.i, %bb17.i.i, %bb15.i.i, %bb14.i.i
  %parent.i.i.1 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %parent.i.i.0, %bb14.i.i ], [ %parent.i.i.0, %bb15.i.i ], [ %136, %bb17.i.i ], [ %136, %bb17.i.i ]
  %p.1.i.i = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %40, %bb14.i.i ], [ %40, %bb15.i.i ], [ %136, %bb17.i.i ], [ %136, %bb17.i.i ]
  %135 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.1.i.i, i32 0, i32 3
  %136 = load %"struct.AVLNode<MemoryBlock,i8*>"** %135, align 4
  %137 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %136, null
  br i1 %137, label %bb34.i.i, label %bb17.i.i

bb34.i.i:                                         ; preds = %bb33.i.i, %bb8.i4.i.i, %bb7.i3.i.i, %_ZN7AVLTreeI11MemoryBlockPcE7rotateLERP7AVLNodeIS0_S1_E.exit.i.i, %bb8.i.i.i, %bb7.i.i.i, %_ZN7AVLTreeI11MemoryBlockPcE7rotateRERP7AVLNodeIS0_S1_E.exit.i.i, %bb17.i.i
  %parent.i.i.2 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %68, %_ZN7AVLTreeI11MemoryBlockPcE7rotateRERP7AVLNodeIS0_S1_E.exit.i.i ], [ %77, %bb7.i.i.i ], [ %77, %bb8.i.i.i ], [ %106, %_ZN7AVLTreeI11MemoryBlockPcE7rotateLERP7AVLNodeIS0_S1_E.exit.i.i ], [ %108, %bb7.i3.i.i ], [ %108, %bb8.i4.i.i ], [ %parent.i.i.1, %bb33.i.i ], [ %136, %bb17.i.i ]
  %138 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %parent.i.i.2, i32 0, i32 3
  %139 = load %"struct.AVLNode<MemoryBlock,i8*>"** %138, align 4
  %140 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %139, null
  br i1 %140, label %bb35.i.i, label %bb36.i.i

bb35.i.i:                                         ; preds = %bb34.i.i
  store %"struct.AVLNode<MemoryBlock,i8*>"* %parent.i.i.2, %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  br label %bb.i.i

bb36.i.i:                                         ; preds = %bb34.i.i
  %141 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %parent.i.i.2, i32 0, i32 0, i32 0
  %142 = load i8** %141, align 4
  %143 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %139, i32 0, i32 0, i32 0
  %144 = load i8** %143, align 4
  %145 = icmp ult i8* %142, %144
  br i1 %145, label %bb38.i.i, label %bb1.i5.i.i

bb1.i5.i.i:                                       ; preds = %bb36.i.i
  %146 = icmp eq i8* %142, %144
  br i1 %146, label %bb2.i6.i.i, label %bb39.i.i

bb2.i6.i.i:                                       ; preds = %bb1.i5.i.i
  %147 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %parent.i.i.2, i32 0, i32 0, i32 1
  %148 = load i32* %147, align 4
  %149 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %139, i32 0, i32 0, i32 1
  %150 = load i32* %149, align 4
  %151 = icmp slt i32 %148, %150
  br i1 %151, label %bb38.i.i, label %bb39.i.i

bb38.i.i:                                         ; preds = %bb2.i6.i.i, %bb36.i.i
  %152 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %139, i32 0, i32 1
  store %"struct.AVLNode<MemoryBlock,i8*>"* %parent.i.i.2, %"struct.AVLNode<MemoryBlock,i8*>"** %152, align 4
  br label %bb.i.i

bb39.i.i:                                         ; preds = %bb2.i6.i.i, %bb1.i5.i.i
  %153 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %139, i32 0, i32 2
  store %"struct.AVLNode<MemoryBlock,i8*>"* %parent.i.i.2, %"struct.AVLNode<MemoryBlock,i8*>"** %153, align 4
  br label %bb.i.i

bb.i.i:                                           ; preds = %bb39.i.i, %bb38.i.i, %bb35.i.i, %bb11.i.i
  %154 = load i32* getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 1), align 4
  %155 = add nsw i32 %154, 1
  store i32 %155, i32* getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 1), align 4
  %.val46.pre = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  br label %mbSetMalloc.exit

mbSetMalloc.exit:                                 ; preds = %bb.i.i, %bb.i.i.i
  %.val46 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %.val46.pre, %bb.i.i ], [ %25, %bb.i.i.i ]
  %156 = bitcast i8* %22 to i32*, !dbg !12
  store i8* %22, i8** %accessedAddress_addr.i4, align 4
  %157 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val46, i8** %accessedAddress_addr.i4) nounwind
  %158 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %157, null
  br i1 %158, label %mbSetAccessed.exit6, label %bb.i5

bb.i5:                                            ; preds = %mbSetMalloc.exit
  %159 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %157, i32 0, i32 0, i32 2
  store i8 1, i8* %159, align 4
  %160 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %157, i32 0, i32 0, i32 1
  %161 = load i32* %160, align 4
  %162 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %157, i32 0, i32 0, i32 0
  %163 = load i8** %162, align 4
  %164 = ptrtoint i8* %163 to i32
  %165 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 8, i32 %164, i32 %161) nounwind
  %.val45.pre = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  br label %mbSetAccessed.exit6

mbSetAccessed.exit6:                              ; preds = %bb.i5, %mbSetMalloc.exit
  %.val45 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %.val46, %mbSetMalloc.exit ], [ %.val45.pre, %bb.i5 ]
  store i8* bitcast (i32** @ptr_int1 to i8*), i8** %accessedAddress_addr.i7, align 4
  %166 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val45, i8** %accessedAddress_addr.i7) nounwind
  %167 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %166, null
  br i1 %167, label %mbSetAccessed.exit9, label %bb.i8

bb.i8:                                            ; preds = %mbSetAccessed.exit6
  %168 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %166, i32 0, i32 0, i32 2
  store i8 1, i8* %168, align 4
  %169 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %166, i32 0, i32 0, i32 1
  %170 = load i32* %169, align 4
  %171 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %166, i32 0, i32 0, i32 0
  %172 = load i8** %171, align 4
  %173 = ptrtoint i8* %172 to i32
  %174 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 8, i32 %173, i32 %170) nounwind
  %.val44.pre = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  br label %mbSetAccessed.exit9

mbSetAccessed.exit9:                              ; preds = %bb.i8, %mbSetAccessed.exit6
  %.val44 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %.val45, %mbSetAccessed.exit6 ], [ %.val44.pre, %bb.i8 ]
  store i32* %156, i32** @ptr_int1, align 4, !dbg !12
  store i8* %1, i8** %accessedAddress_addr.i10, align 4
  %175 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val44, i8** %accessedAddress_addr.i10) nounwind
  %176 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %175, null
  br i1 %176, label %mbSetAccessed.exit12, label %bb.i11

bb.i11:                                           ; preds = %mbSetAccessed.exit9
  %177 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %175, i32 0, i32 0, i32 2
  store i8 1, i8* %177, align 4
  %178 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %175, i32 0, i32 0, i32 1
  %179 = load i32* %178, align 4
  %180 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %175, i32 0, i32 0, i32 0
  %181 = load i8** %180, align 4
  %182 = ptrtoint i8* %181 to i32
  %183 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 11, i32 %182, i32 %179) nounwind
  %.val43.pre = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  br label %mbSetAccessed.exit12

mbSetAccessed.exit12:                             ; preds = %bb.i11, %mbSetAccessed.exit9
  %.val43 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %.val44, %mbSetAccessed.exit9 ], [ %.val43.pre, %bb.i11 ]
  %184 = load i32* %i, align 4, !dbg !13
  %185 = add nsw i32 %184, 1, !dbg !13
  store i8* %1, i8** %accessedAddress_addr.i13, align 4
  %186 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val43, i8** %accessedAddress_addr.i13) nounwind
  %187 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %186, null
  br i1 %187, label %mbSetAccessed.exit15, label %bb.i14

bb.i14:                                           ; preds = %mbSetAccessed.exit12
  %188 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %186, i32 0, i32 0, i32 2
  store i8 1, i8* %188, align 4
  %189 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %186, i32 0, i32 0, i32 1
  %190 = load i32* %189, align 4
  %191 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %186, i32 0, i32 0, i32 0
  %192 = load i8** %191, align 4
  %193 = ptrtoint i8* %192 to i32
  %194 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 11, i32 %193, i32 %190) nounwind
  %.val42.pre = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  br label %mbSetAccessed.exit15

mbSetAccessed.exit15:                             ; preds = %bb.i14, %mbSetAccessed.exit12
  %.val42 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %.val43, %mbSetAccessed.exit12 ], [ %.val42.pre, %bb.i14 ]
  store i32 %185, i32* %i, align 4, !dbg !13
  store i8* bitcast (i32** @ptr_int1 to i8*), i8** %accessedAddress_addr.i16, align 4
  %195 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val42, i8** %accessedAddress_addr.i16) nounwind
  %196 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %195, null
  br i1 %196, label %mbSetAccessed.exit18, label %bb.i17

bb.i17:                                           ; preds = %mbSetAccessed.exit15
  %197 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %195, i32 0, i32 0, i32 2
  store i8 1, i8* %197, align 4
  %198 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %195, i32 0, i32 0, i32 1
  %199 = load i32* %198, align 4
  %200 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %195, i32 0, i32 0, i32 0
  %201 = load i8** %200, align 4
  %202 = ptrtoint i8* %201 to i32
  %203 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 12, i32 %202, i32 %199) nounwind
  %.val41.pre = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  br label %mbSetAccessed.exit18

mbSetAccessed.exit18:                             ; preds = %bb.i17, %mbSetAccessed.exit15
  %.val41 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %.val41.pre, %bb.i17 ], [ %.val42, %mbSetAccessed.exit15 ]
  %204 = load i32** @ptr_int1, align 4, !dbg !14
  %205 = getelementptr inbounds i32* %204, i32 1, !dbg !14
  %206 = bitcast i32* %205 to i8*
  store i8* %206, i8** %accessedAddress_addr.i19, align 4
  %207 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val41, i8** %accessedAddress_addr.i19) nounwind
  %208 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %207, null
  br i1 %208, label %mbSetAccessed.exit21, label %bb.i20

bb.i20:                                           ; preds = %mbSetAccessed.exit18
  %209 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %207, i32 0, i32 0, i32 2
  store i8 1, i8* %209, align 4
  %210 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %207, i32 0, i32 0, i32 1
  %211 = load i32* %210, align 4
  %212 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %207, i32 0, i32 0, i32 0
  %213 = load i8** %212, align 4
  %214 = ptrtoint i8* %213 to i32
  %215 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 12, i32 %214, i32 %211) nounwind
  br label %mbSetAccessed.exit21

mbSetAccessed.exit21:                             ; preds = %bb.i20, %mbSetAccessed.exit18
  store i32 1, i32* %205, align 1, !dbg !14
  br label %bb1, !dbg !14

bb1:                                              ; preds = %mbSetAccessed.exit21, %mbSetAccessed.exit
  store i8* %1, i8** %accessedAddress_addr.i22, align 4
  %.val40 = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  %216 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val40, i8** %accessedAddress_addr.i22) nounwind
  %217 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %216, null
  br i1 %217, label %mbSetAccessed.exit24, label %bb.i23

bb.i23:                                           ; preds = %bb1
  %218 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %216, i32 0, i32 0, i32 2
  store i8 1, i8* %218, align 4
  %219 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %216, i32 0, i32 0, i32 1
  %220 = load i32* %219, align 4
  %221 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %216, i32 0, i32 0, i32 0
  %222 = load i8** %221, align 4
  %223 = ptrtoint i8* %222 to i32
  %224 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 7, i32 %223, i32 %220) nounwind
  br label %mbSetAccessed.exit24

mbSetAccessed.exit24:                             ; preds = %bb.i23, %bb1
  %225 = load i32* %i, align 4, !dbg !15
  %226 = icmp slt i32 %225, 13, !dbg !15
  br i1 %226, label %bb, label %bb2, !dbg !15

bb2:                                              ; preds = %mbSetAccessed.exit24
  %227 = bitcast i32* %0 to i8*
  store i8* %227, i8** %accessedAddress_addr.i25, align 4
  %.val39 = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  %228 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val39, i8** %accessedAddress_addr.i25) nounwind
  %229 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %228, null
  br i1 %229, label %mbSetAccessed.exit27, label %bb.i26

bb.i26:                                           ; preds = %bb2
  %230 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %228, i32 0, i32 0, i32 2
  store i8 1, i8* %230, align 4
  %231 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %228, i32 0, i32 0, i32 1
  %232 = load i32* %231, align 4
  %233 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %228, i32 0, i32 0, i32 0
  %234 = load i8** %233, align 4
  %235 = ptrtoint i8* %234 to i32
  %236 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 20, i32 %235, i32 %232) nounwind
  %.val38.pre = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  br label %mbSetAccessed.exit27

mbSetAccessed.exit27:                             ; preds = %bb.i26, %bb2
  %.val38 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %.val39, %bb2 ], [ %.val38.pre, %bb.i26 ]
  store i32 0, i32* %0, align 4, !dbg !16
  store i8* %227, i8** %accessedAddress_addr.i28, align 4
  %237 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val38, i8** %accessedAddress_addr.i28) nounwind
  %238 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %237, null
  br i1 %238, label %mbSetAccessed.exit30, label %bb.i29

bb.i29:                                           ; preds = %mbSetAccessed.exit27
  %239 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %237, i32 0, i32 0, i32 2
  store i8 1, i8* %239, align 4
  %240 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %237, i32 0, i32 0, i32 1
  %241 = load i32* %240, align 4
  %242 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %237, i32 0, i32 0, i32 0
  %243 = load i8** %242, align 4
  %244 = ptrtoint i8* %243 to i32
  %245 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 20, i32 %244, i32 %241) nounwind
  %.pre57 = load i32* %0, align 4
  %.val37.pre = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  br label %mbSetAccessed.exit30

mbSetAccessed.exit30:                             ; preds = %bb.i29, %mbSetAccessed.exit27
  %.val37 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %.val38, %mbSetAccessed.exit27 ], [ %.val37.pre, %bb.i29 ]
  %246 = phi i32 [ 0, %mbSetAccessed.exit27 ], [ %.pre57, %bb.i29 ]
  %247 = bitcast i32* %retval to i8*
  store i8* %247, i8** %accessedAddress_addr.i31, align 4
  %248 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val37, i8** %accessedAddress_addr.i31) nounwind
  %249 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %248, null
  br i1 %249, label %return, label %bb.i32

bb.i32:                                           ; preds = %mbSetAccessed.exit30
  %250 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %248, i32 0, i32 0, i32 2
  store i8 1, i8* %250, align 4
  %251 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %248, i32 0, i32 0, i32 1
  %252 = load i32* %251, align 4
  %253 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %248, i32 0, i32 0, i32 0
  %254 = load i8** %253, align 4
  %255 = ptrtoint i8* %254 to i32
  %256 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 20, i32 %255, i32 %252) nounwind
  %.val.pre = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  br label %return

return:                                           ; preds = %bb.i32, %mbSetAccessed.exit30
  %.val = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %.val.pre, %bb.i32 ], [ %.val37, %mbSetAccessed.exit30 ]
  store i32 %246, i32* %retval, align 4, !dbg !16
  store i8* %247, i8** %accessedAddress_addr.i34, align 4
  %257 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val, i8** %accessedAddress_addr.i34) nounwind
  %258 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %257, null
  br i1 %258, label %mbSetAccessed.exit36, label %bb.i35

bb.i35:                                           ; preds = %return
  %259 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %257, i32 0, i32 0, i32 2
  store i8 1, i8* %259, align 4
  %260 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %257, i32 0, i32 0, i32 1
  %261 = load i32* %260, align 4
  %262 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %257, i32 0, i32 0, i32 0
  %263 = load i8** %262, align 4
  %264 = ptrtoint i8* %263 to i32
  %265 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 20, i32 %264, i32 %261) nounwind
  %retval3.pre = load i32* %retval, align 4
  br label %mbSetAccessed.exit36

mbSetAccessed.exit36:                             ; preds = %bb.i35, %return
  %retval3 = phi i32 [ %246, %return ], [ %retval3.pre, %bb.i35 ]
  %266 = call i32 @puts(i8* getelementptr inbounds ([19 x i8]* @.str11, i32 0, i32 0)) nounwind
  %267 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* @_ZSt4cout, i8* getelementptr inbounds ([26 x i8]* @.str9, i32 0, i32 0)) nounwind
  %268 = call %"struct.std::ostream"* @_ZNSolsEPFRSoS_E(%"struct.std::ostream"* %267, %"struct.std::ostream"* (%"struct.std::ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  %269 = load i32* getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 1), align 4
  %270 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* @_ZSt4cout, i8* getelementptr inbounds ([19 x i8]* @.str10, i32 0, i32 0)) nounwind
  %271 = call %"struct.std::ostream"* @_ZNSolsEi(%"struct.std::ostream"* %270, i32 %269) nounwind
  %272 = call %"struct.std::ostream"* @_ZNSolsEPFRSoS_E(%"struct.std::ostream"* %271, %"struct.std::ostream"* (%"struct.std::ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  %273 = load %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  call fastcc void @_ZNK7AVLTreeI11MemoryBlockPcE5printEP7AVLNodeIS0_S1_ERSo(%"struct.AVLNode<MemoryBlock,i8*>"* %273) nounwind
  %274 = call %"struct.std::ostream"* @_ZNSolsEPFRSoS_E(%"struct.std::ostream"* @_ZSt4cout, %"struct.std::ostream"* (%"struct.std::ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_) nounwind
  ret i32 %retval3, !dbg !16
}

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

declare noalias i8* @malloc(i32) nounwind

declare i32 @printf(i8*, ...)

define internal void @_GLOBAL__I_mbSetMalloc() {
return:
  call void @_ZNSt8ios_base4InitC1Ev(%"struct.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @__cxa_atexit(void (i8*)* @__tcf_0, i8* null, i8* bitcast (i8** @__dso_handle to i8*)) nounwind
  store %"struct.AVLNode<MemoryBlock,i8*>"* null, %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  store i32 0, i32* getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 1), align 4
  ret void
}

define internal fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %ptr.val, i8** %k) nounwind readonly align 2 {
entry:
  %0 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %ptr.val, null
  br i1 %0, label %return, label %bb1

bb1:                                              ; preds = %entry
  %1 = load i8** %k, align 4
  %2 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr.val, i32 0, i32 0, i32 0
  %3 = load i8** %2, align 4
  %4 = icmp ugt i8* %3, %1
  %.phi.trans.insert = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr.val, i32 0, i32 0, i32 1
  %.pre = load i32* %.phi.trans.insert, align 4
  %.pre1 = getelementptr inbounds i8* %3, i32 %.pre
  %.pre2 = icmp ugt i8* %.pre1, %1
  br i1 %4, label %bb3, label %bb.i

bb.i:                                             ; preds = %bb1
  br i1 %.pre2, label %return, label %bb5

bb3:                                              ; preds = %bb1
  br i1 %.pre2, label %bb6, label %bb5

bb5:                                              ; preds = %bb3, %bb.i
  %5 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr.val, i32 0, i32 2
  %.val = load %"struct.AVLNode<MemoryBlock,i8*>"** %5, align 4
  %6 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val, i8** %k)
  br label %return

bb6:                                              ; preds = %bb3
  %7 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr.val, i32 0, i32 1
  %.val1 = load %"struct.AVLNode<MemoryBlock,i8*>"** %7, align 4
  %8 = call fastcc %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLNode<MemoryBlock,i8*>"* %.val1, i8** %k)
  br label %return

return:                                           ; preds = %bb6, %bb5, %bb.i, %entry
  %.0 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %8, %bb6 ], [ %6, %bb5 ], [ null, %entry ], [ %ptr.val, %bb.i ]
  ret %"struct.AVLNode<MemoryBlock,i8*>"* %.0
}

declare void @_ZNSt8ios_base4InitC1Ev(%"struct.std::ios_base::Init"*)

declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) nounwind

define internal void @__tcf_0(i8* nocapture %unnamed_arg) {
return:
  call void @_ZNSt8ios_base4InitD1Ev(%"struct.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

declare void @_ZNSt8ios_base4InitD1Ev(%"struct.std::ios_base::Init"*)

declare %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"*, i8*)

declare %"struct.std::ostream"* @_ZNSolsEi(%"struct.std::ostream"*, i32)

define internal fastcc void @_ZNK7AVLTreeI11MemoryBlockPcE5printEP7AVLNodeIS0_S1_ERSo(%"struct.AVLNode<MemoryBlock,i8*>"* %ptr) align 2 {
entry:
  %0 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, null
  br i1 %0, label %return, label %bb

bb:                                               ; preds = %entry
  %1 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, i32 0, i32 1
  %2 = load %"struct.AVLNode<MemoryBlock,i8*>"** %1, align 4
  call fastcc void @_ZNK7AVLTreeI11MemoryBlockPcE5printEP7AVLNodeIS0_S1_ERSo(%"struct.AVLNode<MemoryBlock,i8*>"* %2)
  %3 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, i32 0, i32 4
  %4 = load i32* %3, align 4
  %5 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, i32 0, i32 0, i32 3
  %6 = load i8* %5, align 1
  %toBool.i = icmp eq i8 %6, 0
  %storemerge.i = select i1 %toBool.i, i8* getelementptr inbounds ([10 x i8]* @.str1, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str, i32 0, i32 0)
  %7 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, i32 0, i32 0, i32 2
  %8 = load i8* %7, align 4
  %toBool3.i = icmp eq i8 %8, 0
  %storemerge1.i = select i1 %toBool3.i, i8* getelementptr inbounds ([13 x i8]* @.str3, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8]* @.str2, i32 0, i32 0)
  %9 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, i32 0, i32 0, i32 1
  %10 = load i32* %9, align 4
  %11 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, i32 0, i32 0, i32 0
  %12 = load i8** %11, align 4
  %13 = ptrtoint i8* %12 to i32
  %14 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* @_ZSt4cout, i8* getelementptr inbounds ([2 x i8]* @.str4, i32 0, i32 0))
  %15 = call %"struct.std::ostream"* @_ZNSolsEi(%"struct.std::ostream"* %14, i32 %13)
  %16 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %15, i8* getelementptr inbounds ([2 x i8]* @.str5, i32 0, i32 0))
  %17 = call %"struct.std::ostream"* @_ZNSolsEi(%"struct.std::ostream"* %16, i32 %10)
  %18 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %17, i8* getelementptr inbounds ([2 x i8]* @.str5, i32 0, i32 0))
  %19 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %18, i8* %storemerge1.i)
  %20 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %19, i8* getelementptr inbounds ([3 x i8]* @.str6, i32 0, i32 0))
  %21 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %20, i8* %storemerge.i)
  %22 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %21, i8* getelementptr inbounds ([2 x i8]* @.str7, i32 0, i32 0))
  %23 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* @_ZSt4cout, i8* getelementptr inbounds ([6 x i8]* @.str8, i32 0, i32 0))
  %24 = call %"struct.std::ostream"* @_ZNSolsEi(%"struct.std::ostream"* %23, i32 %4)
  %25 = call %"struct.std::ostream"* @_ZNSolsEPFRSoS_E(%"struct.std::ostream"* %24, %"struct.std::ostream"* (%"struct.std::ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  %26 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, i32 0, i32 2
  %27 = load %"struct.AVLNode<MemoryBlock,i8*>"** %26, align 4
  call fastcc void @_ZNK7AVLTreeI11MemoryBlockPcE5printEP7AVLNodeIS0_S1_ERSo(%"struct.AVLNode<MemoryBlock,i8*>"* %27)
  br label %return

return:                                           ; preds = %bb, %entry
  ret void
}

declare %"struct.std::ostream"* @_ZNSolsEPFRSoS_E(%"struct.std::ostream"*, %"struct.std::ostream"* (%"struct.std::ostream"*)*)

declare %"struct.std::ostream"* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_(%"struct.std::ostream"*)

declare i32 @puts(i8*)

declare i8* @_Znwj(i32)

declare void @exit(i32) noreturn nounwind

declare extern_weak i32 @pthread_once(i32*, void ()*)

declare extern_weak i8* @pthread_getspecific(i32)

declare extern_weak i32 @pthread_setspecific(i32, i8*)

declare extern_weak i32 @pthread_create(i32*, %union.pthread_attr_t*, i8* (i8*)*, i8*)

declare extern_weak i32 @pthread_cancel(i32)

declare extern_weak i32 @pthread_mutex_lock(%union.pthread_mutex_t*)

declare extern_weak i32 @pthread_mutex_trylock(%union.pthread_mutex_t*)

declare extern_weak i32 @pthread_mutex_unlock(%union.pthread_mutex_t*)

declare extern_weak i32 @pthread_mutex_init(%union.pthread_mutex_t*, %union..0._10*)

declare extern_weak i32 @pthread_key_create(i32*, void (i8*)*)

declare extern_weak i32 @pthread_key_delete(i32)

declare extern_weak i32 @pthread_mutexattr_init(%union..0._10*)

declare extern_weak i32 @pthread_mutexattr_settype(%union..0._10*, i32)

declare extern_weak i32 @pthread_mutexattr_destroy(%union..0._10*)

!llvm.dbg.sp = !{!0}
!llvm.dbg.gv = !{!6}

!0 = metadata !{i32 589870, i32 0, metadata !1, metadata !"main", metadata !"main", metadata !"main", metadata !1, i32 5, metadata !3, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 ()* @main} ; [ DW_TAG_subprogram ]
!1 = metadata !{i32 589865, metadata !"hello.c", metadata !"/home/zhouyan/work/test/hello/", metadata !2} ; [ DW_TAG_file_type ]
!2 = metadata !{i32 589841, i32 0, i32 1, metadata !"hello.c", metadata !"/home/zhouyan/work/test/hello/", metadata !"4.2.1 (Based on Apple Inc. build 5658) (LLVM build 2.9)", i1 true, i1 false, metadata !"", i32 0} ; [ DW_TAG_compile_unit ]
!3 = metadata !{i32 589845, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !4, i32 0, null} ; [ DW_TAG_subroutine_type ]
!4 = metadata !{metadata !5}
!5 = metadata !{i32 589860, metadata !1, metadata !"int", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!6 = metadata !{i32 589876, i32 0, metadata !1, metadata !"ptr_int1", metadata !"ptr_int1", metadata !"", metadata !1, i32 3, metadata !7, i1 false, i1 true, i32** @ptr_int1} ; [ DW_TAG_variable ]
!7 = metadata !{i32 589839, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !5} ; [ DW_TAG_pointer_type ]
!8 = metadata !{null}
!9 = metadata !{i32 590080, metadata !10, metadata !"i", metadata !1, i32 6, metadata !5, i32 0} ; [ DW_TAG_auto_variable ]
!10 = metadata !{i32 589835, metadata !0, i32 5, i32 0, metadata !1, i32 0} ; [ DW_TAG_lexical_block ]
!11 = metadata !{i32 6, i32 0, metadata !10, null}
!12 = metadata !{i32 8, i32 0, metadata !10, null}
!13 = metadata !{i32 11, i32 0, metadata !10, null}
!14 = metadata !{i32 12, i32 0, metadata !10, null}
!15 = metadata !{i32 7, i32 0, metadata !10, null}
!16 = metadata !{i32 20, i32 0, metadata !10, null}
