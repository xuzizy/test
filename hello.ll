; ModuleID = 'hello.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32-f128:128:128-n8:16:32"
target triple = "i386-pc-linux-gnu"

@ptr_int1 = common unnamed_addr global i32* null

define i32 @main() nounwind {
entry:
  %retval = alloca i32
  %0 = alloca i32
  %i = alloca i32
  %"alloca point" = bitcast i32 0 to i32
  call void @llvm.dbg.declare(metadata !{i32* %i}, metadata !8), !dbg !10
  store i32 0, i32* %i, align 4, !dbg !10
  br label %bb1, !dbg !10

bb:                                               ; preds = %bb1
  %1 = load i32* %i, align 4, !dbg !11
  %2 = add nsw i32 %1, 1, !dbg !11
  %3 = mul nsw i32 %2, 10, !dbg !11
  %4 = call noalias i8* @malloc(i32 %3) nounwind, !dbg !11
  %5 = bitcast i8* %4 to i32*, !dbg !11
  store i32* %5, i32** @ptr_int1, align 4, !dbg !11
  %6 = load i32* %i, align 4, !dbg !12
  %7 = add nsw i32 %6, 1, !dbg !12
  store i32 %7, i32* %i, align 4, !dbg !12
  %8 = load i32** @ptr_int1, align 4, !dbg !13
  %9 = getelementptr inbounds i32* %8, i32 1, !dbg !13
  store i32 1, i32* %9, align 1, !dbg !13
  br label %bb1, !dbg !13

bb1:                                              ; preds = %bb, %entry
  %10 = load i32* %i, align 4, !dbg !14
  %11 = icmp sle i32 %10, 12, !dbg !14
  br i1 %11, label %bb, label %bb2, !dbg !14

bb2:                                              ; preds = %bb1
  store i32 0, i32* %0, align 4, !dbg !15
  %12 = load i32* %0, align 4, !dbg !15
  store i32 %12, i32* %retval, align 4, !dbg !15
  br label %return, !dbg !15

return:                                           ; preds = %bb2
  %retval3 = load i32* %retval, !dbg !15
  ret i32 %retval3, !dbg !15
}

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

declare noalias i8* @malloc(i32) nounwind

!llvm.dbg.sp = !{!0}
!llvm.dbg.gv = !{!6}

!0 = metadata !{i32 589870, i32 0, metadata !1, metadata !"main", metadata !"main", metadata !"main", metadata !1, i32 5, metadata !3, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, i32 ()* @main} ; [ DW_TAG_subprogram ]
!1 = metadata !{i32 589865, metadata !"hello.c", metadata !"/home/zhouyan/work/test/hello/", metadata !2} ; [ DW_TAG_file_type ]
!2 = metadata !{i32 589841, i32 0, i32 1, metadata !"hello.c", metadata !"/home/zhouyan/work/test/hello/", metadata !"4.2.1 (Based on Apple Inc. build 5658) (LLVM build 2.9)", i1 true, i1 false, metadata !"", i32 0} ; [ DW_TAG_compile_unit ]
!3 = metadata !{i32 589845, metadata !1, metadata !"", metadata !1, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !4, i32 0, null} ; [ DW_TAG_subroutine_type ]
!4 = metadata !{metadata !5}
!5 = metadata !{i32 589860, metadata !1, metadata !"int", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!6 = metadata !{i32 589876, i32 0, metadata !1, metadata !"ptr_int1", metadata !"ptr_int1", metadata !"", metadata !1, i32 3, metadata !7, i1 false, i1 true, i32** @ptr_int1} ; [ DW_TAG_variable ]
!7 = metadata !{i32 589839, metadata !1, metadata !"", metadata !1, i32 0, i64 32, i64 32, i64 0, i32 0, metadata !5} ; [ DW_TAG_pointer_type ]
!8 = metadata !{i32 590080, metadata !9, metadata !"i", metadata !1, i32 6, metadata !5, i32 0} ; [ DW_TAG_auto_variable ]
!9 = metadata !{i32 589835, metadata !0, i32 5, i32 0, metadata !1, i32 0} ; [ DW_TAG_lexical_block ]
!10 = metadata !{i32 6, i32 0, metadata !9, null}
!11 = metadata !{i32 8, i32 0, metadata !9, null}
!12 = metadata !{i32 11, i32 0, metadata !9, null}
!13 = metadata !{i32 12, i32 0, metadata !9, null}
!14 = metadata !{i32 7, i32 0, metadata !9, null}
!15 = metadata !{i32 20, i32 0, metadata !9, null}
