#!/bin/bash

make clean

scan-build -analyze-headers -stats -k -V  \
	-enable-checker  debug.DumpCallGraph \
	-enable-checker  debug.DumpCalls \
	-enable-checker  unix.Malloc \
	-enable-checker  debug.ConfigDumper \
	-enable-checker  debug.DumpCFG \
	-enable-checker  debug.DumpCallGraph \
	-enable-checker  debug.DumpCalls \
	-enable-checker  debug.DumpDominators  \
	-enable-checker  debug.DumpLiveVars   \
	-enable-checker  debug.DumpTraversal   \
	-enable-checker  debug.ExprInspection    \
	-enable-checker  debug.Stats           \
	-enable-checker  debug.TaintTest         \
	-enable-checker  debug.ViewCFG             \
	-enable-checker  debug.ViewCallGraph       \
	-enable-checker  llvm.Conventions              \
	-enable-checker  osx.API                     \
	-enable-checker  osx.SecKeychainAPI              \
	-enable-checker  osx.cocoa.AtSync                \
	-enable-checker  osx.cocoa.ClassRelease         \
	-enable-checker  osx.cocoa.IncompatibleMethodTypes \
	-enable-checker  osx.cocoa.Loops              \
	-enable-checker  osx.cocoa.NSAutoreleasePool     \
	-enable-checker  osx.cocoa.NSError    \
	-enable-checker  osx.cocoa.NilArg         \
	-enable-checker  osx.cocoa.NonNilReturnValue   \
	-enable-checker  osx.cocoa.RetainCount          \
	-enable-checker  osx.cocoa.SelfInit         \
	-enable-checker  osx.cocoa.UnusedIvars       \
	-enable-checker  osx.cocoa.VariadicMethodTypes   \
	-enable-checker  osx.coreFoundation.CFError      \
	-enable-checker  osx.coreFoundation.CFNumber    \
	-enable-checker  osx.coreFoundation.CFRetainRelease \
	-enable-checker  osx.coreFoundation.containers.OutOfBounds \
	-enable-checker  osx.coreFoundation.containers.PointerSizedValues \
	-enable-checker   security.FloatLoopCounter      \
	-enable-checker alpha.unix.MallocWithAnnotations  \
-enable-checker unix.Malloc -o ./report  make
