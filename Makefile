MYLIB=../../llvm-2.9/lib/Transforms/MyInstrumentation/mylib
MYPASS=../../llvm-2.9/Release+Asserts/lib

all:hello_final.ll hello.ll hello_t.ll mem_track.ll AVLTree.ll

AVLTree.ll:$(MYLIB)/AVLTree.bc
	llvm-dis $(MYLIB)/AVLTree.bc -o AVLTree.ll

hello_final.ll:hello_final.bc
	llvm-dis hello_final.bc 

hello.ll:hello.bc
	llvm-dis hello.bc

hello_t.ll:hello_t.bc
	llvm-dis hello_t.bc

mem_track.ll:$(MYLIB)/mem_track.bc
	llvm-dis $(MYLIB)/mem_track.bc -o mem_track.ll

hello_final hello_final.bc:hello_t.bc $(MYLIB)/mem_track.bc  
	llvm-ld hello_t.bc  $(MYLIB)/mem_track.bc -v -o hello_final

hello_t.bc:hello.bc $(MYPASS)/MyInstrumentation.so
	echo "run MyInstrumentation------------------------------------------------- "
	opt -load $(MYPASS)/MyInstrumentation.so -MyInstrumentation < hello.bc > hello_t.bc

$(MYPASS)/MyInstrumentation.so:$(MYLIB)/../MyInstrumentation.cpp
	echo "make  MyInstrumentation---------------------------------------------- "
	make -C $(MYLIB)/..

#-g is necessage for print line number in the program
hello.bc:hello.c
	llvm-gcc -g -emit-llvm -c hello.c -o hello.bc -Wall -Werror 

$(MYLIB)/mem_track.bc:
	echo "make mylib ---------------------------------------------------------------- "
	make -C $(MYLIB)

clean:
	rm -f *.bc
	rm -f hello_final
	rm -f *.ll
	make -C $(MYLIB)/.. clean
	make -C $(MYLIB) clean
