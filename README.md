It is a test case of the llvm pass MyInstrumentation.

File description:
     
*.c    测试源代码


*.bc   中间代码


*.ll   中间代码的人类可读形式


output.txt: 最终代码的输出


To run the hello_final.bc using lli :

lli hello_final.bc

To run the hello_final directly:

./hello_final


Attention: the llvm being used is version 2.9,in order to compatible with KLEE(KLEE can only support llvm 2.5~2.9)
