; ModuleID = '../../llvm-2.9/lib/Transforms/MyInstrumentation/mylib/mem_track.bc'
target datalayout = "e-p:32:32:32-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:32:64-f32:32:32-f64:32:64-v64:64:64-v128:128:128-a0:0:64-f80:32:32-f128:128:128-n8:16:32"
target triple = "i386-pc-linux-gnu"

%0 = type { i32, void ()* }
%struct..1__pthread_mutex_s = type { i32, i32, i32, i32, i32, %union..0._10 }
%"struct.AVLNode<MemoryBlock,i8*>" = type { %struct.MemoryBlock, %"struct.AVLNode<MemoryBlock,i8*>"*, %"struct.AVLNode<MemoryBlock,i8*>"*, %"struct.AVLNode<MemoryBlock,i8*>"*, i32 }
%"struct.AVLTree<MemoryBlock,i8*>" = type { %"struct.AVLNode<MemoryBlock,i8*>"*, i32 }
%struct.MemoryBlock = type { i8*, i32, i8, i8 }
%struct.__pthread_slist_t = type { %struct.__pthread_slist_t* }
%"struct.std::basic_ios<char,std::char_traits<char> >" = type { %"struct.std::ios_base", %"struct.std::ostream"*, i8, i8, %"struct.std::basic_streambuf<char,std::char_traits<char> >"*, %"struct.std::ctype<char>"*, %"struct.std::num_get<char,std::istreambuf_iterator<char, std::char_traits<char> > >"*, %"struct.std::num_get<char,std::istreambuf_iterator<char, std::char_traits<char> > >"* }
%"struct.std::basic_streambuf<char,std::char_traits<char> >" = type { i32 (...)**, i8*, i8*, i8*, i8*, i8*, i8*, %"struct.std::locale" }
%"struct.std::ctype<char>" = type { %"struct.std::locale::facet", i32*, i8, i32*, i32*, i16*, i8, [256 x i8], [256 x i8], i8 }
%"struct.std::ios_base" = type { i32 (...)**, i32, i32, i32, i32, i32, %"struct.std::ios_base::_Callback_list"*, %"struct.std::ios_base::_Words", [8 x %"struct.std::ios_base::_Words"], i32, %"struct.std::ios_base::_Words"*, %"struct.std::locale" }
%"struct.std::ios_base::Init" = type <{ i8 }>
%"struct.std::ios_base::_Callback_list" = type { %"struct.std::ios_base::_Callback_list"*, void (i32, %"struct.std::ios_base"*, i32)*, i32, i32 }
%"struct.std::ios_base::_Words" = type { i8*, i32 }
%"struct.std::locale" = type { %"struct.std::locale::_Impl"* }
%"struct.std::locale::_Impl" = type { i32, %"struct.std::locale::facet"**, i32, %"struct.std::locale::facet"**, i8** }
%"struct.std::locale::facet" = type { i32 (...)**, i32 }
%"struct.std::num_get<char,std::istreambuf_iterator<char, std::char_traits<char> > >" = type { %"struct.std::locale::facet" }
%"struct.std::num_put<char,std::ostreambuf_iterator<char, std::char_traits<char> > >" = type { %"struct.std::locale::facet" }
%"struct.std::ostream" = type { i32 (...)**, %"struct.std::basic_ios<char,std::char_traits<char> >" }
%union..0._10 = type { i32 }
%union.pthread_attr_t = type { i32, [8 x i32] }
%union.pthread_mutex_t = type { %struct..1__pthread_mutex_s }
%union.pthread_mutexattr_t = type { i32 }

@_ZStL8__ioinit = internal global %"struct.std::ios_base::Init" zeroinitializer
@__dso_handle = external global i8*
@_ZL6mbTree = internal global %"struct.AVLTree<MemoryBlock,i8*>" zeroinitializer
@.str = private constant [6 x i8] c"freed\00", align 1
@.str1 = private constant [10 x i8] c"not freed\00", align 1
@.str2 = private constant [9 x i8] c"accessed\00", align 1
@.str3 = private constant [13 x i8] c"not accessed\00", align 1
@.str4 = private constant [2 x i8] c"[\00", align 1
@.str5 = private constant [2 x i8] c",\00", align 1
@.str6 = private constant [3 x i8] c", \00", align 1
@.str7 = private constant [2 x i8] c"]\00", align 1
@.str8 = private constant [6 x i8] c" bf: \00", align 1
@.str9 = private constant [26 x i8] c"AVL Tree print(inorder): \00", align 1
@.str10 = private constant [19 x i8] c"Number of Nodes : \00", align 1
@.str11 = private constant [19 x i8] c"memory block list:\00", align 1
@_ZSt4cout = external global %"struct.std::ostream"
@.str12 = private constant [44 x i8] c"At line %d: accesse memory block [%u,%u]  \0A\00", align 4
@.str13 = private constant [55 x i8] c"At line %d:  free(%d) call,instrument mbSetFreed (%d)\0A\00", align 4
@_ZSt4cerr = external global %"struct.std::ostream"
@.str14 = private constant [31 x i8] c"new AVLNode failed in insert()\00", align 4
@.str15 = private constant [59 x i8] c"At line %d:  %d=malloc(%d),instrument mbSetMalloc (%d,%d)\0A\00", align 4
@llvm.global_ctors = appending global [1 x %0] [%0 { i32 65535, void ()* @_GLOBAL__I_mbSetMalloc }]

@_ZL20__gthrw_pthread_oncePiPFvvE = alias weak i32 (i32*, void ()*)* @pthread_once
@_ZL27__gthrw_pthread_getspecificj = alias weak i8* (i32)* @pthread_getspecific
@_ZL27__gthrw_pthread_setspecificjPKv = alias weak i32 (i32, i8*)* @pthread_setspecific
@_ZL22__gthrw_pthread_createPmPK14pthread_attr_tPFPvS3_ES3_ = alias weak i32 (i32*, %union.pthread_attr_t*, i8* (i8*)*, i8*)* @pthread_create
@_ZL22__gthrw_pthread_cancelm = alias weak i32 (i32)* @pthread_cancel
@_ZL26__gthrw_pthread_mutex_lockP15pthread_mutex_t = alias weak i32 (%union.pthread_mutex_t*)* @pthread_mutex_lock
@_ZL29__gthrw_pthread_mutex_trylockP15pthread_mutex_t = alias weak i32 (%union.pthread_mutex_t*)* @pthread_mutex_trylock
@_ZL28__gthrw_pthread_mutex_unlockP15pthread_mutex_t = alias weak i32 (%union.pthread_mutex_t*)* @pthread_mutex_unlock
@_ZL26__gthrw_pthread_mutex_initP15pthread_mutex_tPK19pthread_mutexattr_t = alias weak i32 (%union.pthread_mutex_t*, %union..0._10*)* @pthread_mutex_init
@_ZL26__gthrw_pthread_key_createPjPFvPvE = alias weak i32 (i32*, void (i8*)*)* @pthread_key_create
@_ZL26__gthrw_pthread_key_deletej = alias weak i32 (i32)* @pthread_key_delete
@_ZL30__gthrw_pthread_mutexattr_initP19pthread_mutexattr_t = alias weak i32 (%union..0._10*)* @pthread_mutexattr_init
@_ZL33__gthrw_pthread_mutexattr_settypeP19pthread_mutexattr_ti = alias weak i32 (%union..0._10*, i32)* @pthread_mutexattr_settype
@_ZL33__gthrw_pthread_mutexattr_destroyP19pthread_mutexattr_t = alias weak i32 (%union..0._10*)* @pthread_mutexattr_destroy

define i32 @mbSetMalloc(i8* %memStart, i32 %memSize, i32 %lineNum) {
entry:
  %0 = alloca %struct.MemoryBlock, align 8
  %1 = ptrtoint i8* %memStart to i32
  %2 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([59 x i8]* @.str15, i32 0, i32 0), i32 %lineNum, i32 %1, i32 %memSize, i32 %1, i32 %memSize)
  %3 = getelementptr inbounds %struct.MemoryBlock* %0, i32 0, i32 0
  store i8* %memStart, i8** %3, align 8
  %4 = getelementptr inbounds %struct.MemoryBlock* %0, i32 0, i32 1
  store i32 %memSize, i32* %4, align 4
  %5 = getelementptr inbounds %struct.MemoryBlock* %0, i32 0, i32 2
  store i8 0, i8* %5, align 8
  %6 = getelementptr inbounds %struct.MemoryBlock* %0, i32 0, i32 3
  store i8 0, i8* %6, align 1
  %7 = call zeroext i8 @_ZN7AVLTreeI11MemoryBlockPcE6insertERP7AVLNodeIS0_S1_ERKS0_(%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), %struct.MemoryBlock* %0)
  %toBool.i = icmp eq i8 %7, 0
  br i1 %toBool.i, label %return, label %bb.i

bb.i:                                             ; preds = %entry
  %8 = load i32* getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 1), align 4
  %9 = add nsw i32 %8, 1
  store i32 %9, i32* getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 1), align 4
  br label %return

return:                                           ; preds = %bb.i, %entry
  ret i32 1
}

define internal void @_GLOBAL__I_mbSetMalloc() {
return:
  call void @_ZNSt8ios_base4InitC1Ev(%"struct.std::ios_base::Init"* @_ZStL8__ioinit)
  %0 = call i32 @__cxa_atexit(void (i8*)* @__tcf_0, i8* null, i8* bitcast (i8** @__dso_handle to i8*)) nounwind
  store %"struct.AVLNode<MemoryBlock,i8*>"* null, %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), align 8
  store i32 0, i32* getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 1), align 4
  ret void
}

define linkonce_odr %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLTree<MemoryBlock,i8*>"* %this, %"struct.AVLNode<MemoryBlock,i8*>"** nocapture %ptr, i8** %k) nounwind readonly align 2 {
entry:
  %0 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %1 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %0, null
  br i1 %1, label %return, label %bb1

bb1:                                              ; preds = %entry
  %2 = load i8** %k, align 4
  %3 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 0, i32 0
  %4 = load i8** %3, align 4
  %5 = icmp ugt i8* %4, %2
  %.phi.trans.insert = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 0, i32 1
  %.pre = load i32* %.phi.trans.insert, align 4
  br i1 %5, label %bb3, label %bb.i

bb.i:                                             ; preds = %bb1
  %6 = getelementptr inbounds i8* %4, i32 %.pre
  %7 = icmp ugt i8* %6, %2
  br i1 %7, label %return, label %bb3

bb3:                                              ; preds = %bb.i, %bb1
  %8 = getelementptr inbounds i8* %4, i32 %.pre
  %9 = icmp ugt i8* %8, %2
  br i1 %9, label %bb6, label %bb5

bb5:                                              ; preds = %bb3
  %10 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 2
  %11 = call %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLTree<MemoryBlock,i8*>"* %this, %"struct.AVLNode<MemoryBlock,i8*>"** %10, i8** %k)
  br label %return

bb6:                                              ; preds = %bb3
  %12 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 1
  %13 = call %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLTree<MemoryBlock,i8*>"* %this, %"struct.AVLNode<MemoryBlock,i8*>"** %12, i8** %k)
  br label %return

return:                                           ; preds = %bb6, %bb5, %bb.i, %entry
  %.0 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %13, %bb6 ], [ %11, %bb5 ], [ null, %entry ], [ %0, %bb.i ]
  ret %"struct.AVLNode<MemoryBlock,i8*>"* %.0
}

define linkonce_odr void @_ZN7AVLTreeI11MemoryBlockPcE8rotateLRERP7AVLNodeIS0_S1_E(%"struct.AVLTree<MemoryBlock,i8*>"* nocapture %this, %"struct.AVLNode<MemoryBlock,i8*>"** nocapture %ptr) nounwind align 2 {
entry:
  %0 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %1 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 1
  %2 = load %"struct.AVLNode<MemoryBlock,i8*>"** %1, align 4
  %3 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %2, i32 0, i32 2
  %4 = load %"struct.AVLNode<MemoryBlock,i8*>"** %3, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %4, %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %5 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %4, i32 0, i32 1
  %6 = load %"struct.AVLNode<MemoryBlock,i8*>"** %5, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %6, %"struct.AVLNode<MemoryBlock,i8*>"** %3, align 4
  %7 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %8 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %7, i32 0, i32 1
  store %"struct.AVLNode<MemoryBlock,i8*>"* %2, %"struct.AVLNode<MemoryBlock,i8*>"** %8, align 4
  %9 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %10 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %9, i32 0, i32 4
  %11 = load i32* %10, align 4
  %12 = icmp slt i32 %11, 1
  %13 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %2, i32 0, i32 4
  %. = select i1 %12, i32 0, i32 -1
  store i32 %., i32* %13, align 4
  %14 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %15 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %14, i32 0, i32 2
  %16 = load %"struct.AVLNode<MemoryBlock,i8*>"** %15, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %16, %"struct.AVLNode<MemoryBlock,i8*>"** %1, align 4
  %17 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %18 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %17, i32 0, i32 2
  store %"struct.AVLNode<MemoryBlock,i8*>"* %0, %"struct.AVLNode<MemoryBlock,i8*>"** %18, align 4
  %19 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %20 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %19, i32 0, i32 4
  %21 = load i32* %20, align 4
  %22 = icmp eq i32 %21, -1
  %23 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 4
  %storemerge1 = select i1 %22, i32 1, i32 0
  store i32 %storemerge1, i32* %23, align 4
  %24 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %25 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %24, i32 0, i32 4
  store i32 0, i32* %25, align 4
  %26 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %27 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 3
  %28 = load %"struct.AVLNode<MemoryBlock,i8*>"** %27, align 4
  %29 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %26, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %28, %"struct.AVLNode<MemoryBlock,i8*>"** %29, align 4
  %30 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %30, %"struct.AVLNode<MemoryBlock,i8*>"** %27, align 4
  %31 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %2, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %30, %"struct.AVLNode<MemoryBlock,i8*>"** %31, align 4
  %32 = load %"struct.AVLNode<MemoryBlock,i8*>"** %3, align 4
  %33 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %32, null
  br i1 %33, label %bb7, label %bb6

bb6:                                              ; preds = %entry
  %34 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %32, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %2, %"struct.AVLNode<MemoryBlock,i8*>"** %34, align 4
  br label %bb7

bb7:                                              ; preds = %bb6, %entry
  %35 = load %"struct.AVLNode<MemoryBlock,i8*>"** %1, align 4
  %36 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %35, null
  br i1 %36, label %return, label %bb8

bb8:                                              ; preds = %bb7
  %37 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %35, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %0, %"struct.AVLNode<MemoryBlock,i8*>"** %37, align 4
  br label %return

return:                                           ; preds = %bb8, %bb7
  ret void
}

define linkonce_odr void @_ZN7AVLTreeI11MemoryBlockPcE8rotateRLERP7AVLNodeIS0_S1_E(%"struct.AVLTree<MemoryBlock,i8*>"* nocapture %this, %"struct.AVLNode<MemoryBlock,i8*>"** nocapture %ptr) nounwind align 2 {
entry:
  %0 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %1 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 2
  %2 = load %"struct.AVLNode<MemoryBlock,i8*>"** %1, align 4
  %3 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %2, i32 0, i32 1
  %4 = load %"struct.AVLNode<MemoryBlock,i8*>"** %3, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %4, %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %5 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %4, i32 0, i32 2
  %6 = load %"struct.AVLNode<MemoryBlock,i8*>"** %5, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %6, %"struct.AVLNode<MemoryBlock,i8*>"** %3, align 4
  %7 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %8 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %7, i32 0, i32 2
  store %"struct.AVLNode<MemoryBlock,i8*>"* %2, %"struct.AVLNode<MemoryBlock,i8*>"** %8, align 4
  %9 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %10 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %9, i32 0, i32 4
  %11 = load i32* %10, align 4
  %12 = icmp sgt i32 %11, -1
  %13 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %2, i32 0, i32 4
  %. = select i1 %12, i32 0, i32 1
  store i32 %., i32* %13, align 4
  %14 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %15 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %14, i32 0, i32 1
  %16 = load %"struct.AVLNode<MemoryBlock,i8*>"** %15, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %16, %"struct.AVLNode<MemoryBlock,i8*>"** %1, align 4
  %17 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %18 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %17, i32 0, i32 1
  store %"struct.AVLNode<MemoryBlock,i8*>"* %0, %"struct.AVLNode<MemoryBlock,i8*>"** %18, align 4
  %19 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %20 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %19, i32 0, i32 4
  %21 = load i32* %20, align 4
  %22 = icmp eq i32 %21, 1
  %23 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 4
  %storemerge1 = select i1 %22, i32 -1, i32 0
  store i32 %storemerge1, i32* %23, align 4
  %24 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %25 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %24, i32 0, i32 4
  store i32 0, i32* %25, align 4
  %26 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %27 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 3
  %28 = load %"struct.AVLNode<MemoryBlock,i8*>"** %27, align 4
  %29 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %26, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %28, %"struct.AVLNode<MemoryBlock,i8*>"** %29, align 4
  %30 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  %31 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %2, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %30, %"struct.AVLNode<MemoryBlock,i8*>"** %31, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %30, %"struct.AVLNode<MemoryBlock,i8*>"** %27, align 4
  %32 = load %"struct.AVLNode<MemoryBlock,i8*>"** %1, align 4
  %33 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %32, null
  br i1 %33, label %bb7, label %bb6

bb6:                                              ; preds = %entry
  %34 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %32, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %0, %"struct.AVLNode<MemoryBlock,i8*>"** %34, align 4
  br label %bb7

bb7:                                              ; preds = %bb6, %entry
  %35 = load %"struct.AVLNode<MemoryBlock,i8*>"** %3, align 4
  %36 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %35, null
  br i1 %36, label %return, label %bb8

bb8:                                              ; preds = %bb7
  %37 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %35, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %2, %"struct.AVLNode<MemoryBlock,i8*>"** %37, align 4
  br label %return

return:                                           ; preds = %bb8, %bb7
  ret void
}

declare void @_ZNSt8ios_base4InitC1Ev(%"struct.std::ios_base::Init"*)

declare i32 @__cxa_atexit(void (i8*)*, i8*, i8*) nounwind

define internal void @__tcf_0(i8* nocapture %unnamed_arg) {
return:
  call void @_ZNSt8ios_base4InitD1Ev(%"struct.std::ios_base::Init"* @_ZStL8__ioinit)
  ret void
}

declare void @_ZNSt8ios_base4InitD1Ev(%"struct.std::ios_base::Init"*)

define linkonce_odr %"struct.std::ostream"* @_ZlsRSoRK11MemoryBlock(%"struct.std::ostream"* %out, %struct.MemoryBlock* nocapture %mb) {
entry:
  %0 = getelementptr inbounds %struct.MemoryBlock* %mb, i32 0, i32 3
  %1 = load i8* %0, align 1
  %toBool = icmp eq i8 %1, 0
  %storemerge = select i1 %toBool, i8* getelementptr inbounds ([10 x i8]* @.str1, i32 0, i32 0), i8* getelementptr inbounds ([6 x i8]* @.str, i32 0, i32 0)
  %2 = getelementptr inbounds %struct.MemoryBlock* %mb, i32 0, i32 2
  %3 = load i8* %2, align 4
  %toBool3 = icmp eq i8 %3, 0
  %storemerge1 = select i1 %toBool3, i8* getelementptr inbounds ([13 x i8]* @.str3, i32 0, i32 0), i8* getelementptr inbounds ([9 x i8]* @.str2, i32 0, i32 0)
  %4 = getelementptr inbounds %struct.MemoryBlock* %mb, i32 0, i32 1
  %5 = load i32* %4, align 4
  %6 = getelementptr inbounds %struct.MemoryBlock* %mb, i32 0, i32 0
  %7 = load i8** %6, align 4
  %8 = ptrtoint i8* %7 to i32
  %9 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %out, i8* getelementptr inbounds ([2 x i8]* @.str4, i32 0, i32 0))
  %10 = call %"struct.std::ostream"* @_ZNSolsEi(%"struct.std::ostream"* %9, i32 %8)
  %11 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %10, i8* getelementptr inbounds ([2 x i8]* @.str5, i32 0, i32 0))
  %12 = call %"struct.std::ostream"* @_ZNSolsEi(%"struct.std::ostream"* %11, i32 %5)
  %13 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %12, i8* getelementptr inbounds ([2 x i8]* @.str5, i32 0, i32 0))
  %14 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %13, i8* %storemerge1)
  %15 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %14, i8* getelementptr inbounds ([3 x i8]* @.str6, i32 0, i32 0))
  %16 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %15, i8* %storemerge)
  %17 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %16, i8* getelementptr inbounds ([2 x i8]* @.str7, i32 0, i32 0))
  ret %"struct.std::ostream"* %out
}

declare %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"*, i8*)

declare %"struct.std::ostream"* @_ZNSolsEi(%"struct.std::ostream"*, i32)

define linkonce_odr void @_ZNK7AVLTreeI11MemoryBlockPcE5printEP7AVLNodeIS0_S1_ERSo(%"struct.AVLTree<MemoryBlock,i8*>"* %this, %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, %"struct.std::ostream"* %out) align 2 {
entry:
  %0 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, null
  br i1 %0, label %return, label %bb

bb:                                               ; preds = %entry
  %1 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, i32 0, i32 1
  %2 = load %"struct.AVLNode<MemoryBlock,i8*>"** %1, align 4
  call void @_ZNK7AVLTreeI11MemoryBlockPcE5printEP7AVLNodeIS0_S1_ERSo(%"struct.AVLTree<MemoryBlock,i8*>"* %this, %"struct.AVLNode<MemoryBlock,i8*>"* %2, %"struct.std::ostream"* %out)
  %3 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, i32 0, i32 4
  %4 = load i32* %3, align 4
  %5 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, i32 0, i32 0
  %6 = call %"struct.std::ostream"* @_ZlsRSoRK11MemoryBlock(%"struct.std::ostream"* %out, %struct.MemoryBlock* %5)
  %7 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %6, i8* getelementptr inbounds ([6 x i8]* @.str8, i32 0, i32 0))
  %8 = call %"struct.std::ostream"* @_ZNSolsEi(%"struct.std::ostream"* %7, i32 %4)
  %9 = call %"struct.std::ostream"* @_ZNSolsEPFRSoS_E(%"struct.std::ostream"* %8, %"struct.std::ostream"* (%"struct.std::ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  %10 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %ptr, i32 0, i32 2
  %11 = load %"struct.AVLNode<MemoryBlock,i8*>"** %10, align 4
  call void @_ZNK7AVLTreeI11MemoryBlockPcE5printEP7AVLNodeIS0_S1_ERSo(%"struct.AVLTree<MemoryBlock,i8*>"* %this, %"struct.AVLNode<MemoryBlock,i8*>"* %11, %"struct.std::ostream"* %out)
  br label %return

return:                                           ; preds = %bb, %entry
  ret void
}

declare %"struct.std::ostream"* @_ZNSolsEPFRSoS_E(%"struct.std::ostream"*, %"struct.std::ostream"* (%"struct.std::ostream"*)*)

declare %"struct.std::ostream"* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_(%"struct.std::ostream"*)

define linkonce_odr %"struct.std::ostream"* @_ZlsI11MemoryBlockPcERSoS2_RK7AVLTreeIT_T0_E(%"struct.std::ostream"* %out, %"struct.AVLTree<MemoryBlock,i8*>"* %tree) {
return:
  %0 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %out, i8* getelementptr inbounds ([26 x i8]* @.str9, i32 0, i32 0))
  %1 = call %"struct.std::ostream"* @_ZNSolsEPFRSoS_E(%"struct.std::ostream"* %0, %"struct.std::ostream"* (%"struct.std::ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  %2 = getelementptr inbounds %"struct.AVLTree<MemoryBlock,i8*>"* %tree, i32 0, i32 1
  %3 = load i32* %2, align 4
  %4 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* %out, i8* getelementptr inbounds ([19 x i8]* @.str10, i32 0, i32 0))
  %5 = call %"struct.std::ostream"* @_ZNSolsEi(%"struct.std::ostream"* %4, i32 %3)
  %6 = call %"struct.std::ostream"* @_ZNSolsEPFRSoS_E(%"struct.std::ostream"* %5, %"struct.std::ostream"* (%"struct.std::ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  %7 = getelementptr inbounds %"struct.AVLTree<MemoryBlock,i8*>"* %tree, i32 0, i32 0
  %8 = load %"struct.AVLNode<MemoryBlock,i8*>"** %7, align 4
  call void @_ZNK7AVLTreeI11MemoryBlockPcE5printEP7AVLNodeIS0_S1_ERSo(%"struct.AVLTree<MemoryBlock,i8*>"* %tree, %"struct.AVLNode<MemoryBlock,i8*>"* %8, %"struct.std::ostream"* %out)
  ret %"struct.std::ostream"* %out
}

define i32 @mbPrint() {
return:
  %0 = call i32 @puts(i8* getelementptr inbounds ([19 x i8]* @.str11, i32 0, i32 0))
  %1 = call %"struct.std::ostream"* @_ZlsI11MemoryBlockPcERSoS2_RK7AVLTreeIT_T0_E(%"struct.std::ostream"* @_ZSt4cout, %"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree)
  %2 = call %"struct.std::ostream"* @_ZNSolsEPFRSoS_E(%"struct.std::ostream"* %1, %"struct.std::ostream"* (%"struct.std::ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  ret i32 0
}

declare i32 @puts(i8*)

define i32 @mbSetAccessed(i8* %accessedAddress, i32 %lineNum) {
entry:
  %accessedAddress_addr = alloca i8*, align 4
  store i8* %accessedAddress, i8** %accessedAddress_addr, align 4
  %0 = call %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), i8** %accessedAddress_addr) nounwind
  %1 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %0, null
  br i1 %1, label %return, label %bb

bb:                                               ; preds = %entry
  %2 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 0, i32 2
  store i8 1, i8* %2, align 4
  %3 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 0, i32 1
  %4 = load i32* %3, align 4
  %5 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 0, i32 0
  %6 = load i8** %5, align 4
  %7 = ptrtoint i8* %6 to i32
  %8 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([44 x i8]* @.str12, i32 0, i32 0), i32 %lineNum, i32 %7, i32 %4)
  br label %return

return:                                           ; preds = %bb, %entry
  %storemerge = phi i32 [ 1, %bb ], [ 0, %entry ]
  ret i32 %storemerge
}

declare i32 @printf(i8* noalias, ...)

define i32 @mbSetFreed(i8* %memStart, i32 %lineNum) {
entry:
  %memStart_addr = alloca i8*, align 4
  store i8* %memStart, i8** %memStart_addr, align 4
  %0 = call %"struct.AVLNode<MemoryBlock,i8*>"* @_ZN7AVLTreeI11MemoryBlockPcE6searchERP7AVLNodeIS0_S1_ERKS1_(%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, %"struct.AVLNode<MemoryBlock,i8*>"** getelementptr inbounds (%"struct.AVLTree<MemoryBlock,i8*>"* @_ZL6mbTree, i32 0, i32 0), i8** %memStart_addr) nounwind
  %1 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %0, null
  br i1 %1, label %return, label %bb

bb:                                               ; preds = %entry
  %2 = ptrtoint i8* %memStart to i32
  %3 = call i32 (i8*, ...)* @printf(i8* noalias getelementptr inbounds ([55 x i8]* @.str13, i32 0, i32 0), i32 %lineNum, i32 %2, i32 %2)
  %4 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %0, i32 0, i32 0, i32 3
  store i8 1, i8* %4, align 1
  br label %return

return:                                           ; preds = %bb, %entry
  %storemerge = phi i32 [ 1, %bb ], [ 0, %entry ]
  ret i32 %storemerge
}

define linkonce_odr zeroext i8 @_ZN7AVLTreeI11MemoryBlockPcE6insertERP7AVLNodeIS0_S1_ERKS0_(%"struct.AVLTree<MemoryBlock,i8*>"* nocapture %this, %"struct.AVLNode<MemoryBlock,i8*>"** nocapture %ptr, %struct.MemoryBlock* nocapture %e) align 2 {
entry:
  %parent = alloca %"struct.AVLNode<MemoryBlock,i8*>"*, align 4
  %0 = load %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* null, %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  %1 = getelementptr inbounds %struct.MemoryBlock* %e, i32 0, i32 0
  %2 = getelementptr inbounds %struct.MemoryBlock* %e, i32 0, i32 1
  br label %bb6

bb:                                               ; preds = %bb6
  %3 = load i8** %1, align 4
  %4 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.0, i32 0, i32 0, i32 0
  %5 = load i8** %4, align 4
  %6 = icmp eq i8* %3, %5
  br i1 %6, label %bb.i, label %bb2

bb.i:                                             ; preds = %bb
  %7 = load i32* %2, align 4
  %8 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.0, i32 0, i32 0, i32 1
  %9 = load i32* %8, align 4
  %10 = icmp eq i32 %7, %9
  br i1 %10, label %return, label %bb2

bb2:                                              ; preds = %bb.i, %bb
  store %"struct.AVLNode<MemoryBlock,i8*>"* %p.0, %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  %11 = load i8** %4, align 4
  %12 = icmp ult i8* %3, %11
  br i1 %12, label %bb4, label %bb1.i11

bb1.i11:                                          ; preds = %bb2
  %13 = icmp eq i8* %3, %11
  br i1 %13, label %bb2.i12, label %bb5

bb2.i12:                                          ; preds = %bb1.i11
  %14 = load i32* %2, align 4
  %15 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.0, i32 0, i32 0, i32 1
  %16 = load i32* %15, align 4
  %17 = icmp slt i32 %14, %16
  br i1 %17, label %bb4, label %bb5

bb4:                                              ; preds = %bb2.i12, %bb2
  %18 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.0, i32 0, i32 1
  %19 = load %"struct.AVLNode<MemoryBlock,i8*>"** %18, align 4
  br label %bb6

bb5:                                              ; preds = %bb2.i12, %bb1.i11
  %20 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.0, i32 0, i32 2
  %21 = load %"struct.AVLNode<MemoryBlock,i8*>"** %20, align 4
  br label %bb6

bb6:                                              ; preds = %bb5, %bb4, %entry
  %p.0 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %0, %entry ], [ %21, %bb5 ], [ %19, %bb4 ]
  %22 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %p.0, null
  br i1 %22, label %bb7, label %bb

bb7:                                              ; preds = %bb6
  %23 = call i8* @_Znwj(i32 28)
  %24 = bitcast i8* %23 to %"struct.AVLNode<MemoryBlock,i8*>"*
  %25 = bitcast i8* %23 to i8**
  %26 = load i8** %1, align 4
  store i8* %26, i8** %25, align 4
  %27 = getelementptr inbounds i8* %23, i32 4
  %28 = bitcast i8* %27 to i32*
  %29 = load i32* %2, align 4
  store i32 %29, i32* %28, align 4
  %30 = getelementptr inbounds i8* %23, i32 8
  %31 = getelementptr inbounds %struct.MemoryBlock* %e, i32 0, i32 2
  %32 = load i8* %31, align 4
  store i8 %32, i8* %30, align 4
  %33 = getelementptr inbounds i8* %23, i32 9
  %34 = getelementptr inbounds %struct.MemoryBlock* %e, i32 0, i32 3
  %35 = load i8* %34, align 1
  store i8 %35, i8* %33, align 1
  %36 = getelementptr inbounds i8* %23, i32 12
  %37 = bitcast i8* %36 to %"struct.AVLNode<MemoryBlock,i8*>"**
  store %"struct.AVLNode<MemoryBlock,i8*>"* null, %"struct.AVLNode<MemoryBlock,i8*>"** %37, align 4
  %38 = getelementptr inbounds i8* %23, i32 16
  %39 = bitcast i8* %38 to %"struct.AVLNode<MemoryBlock,i8*>"**
  store %"struct.AVLNode<MemoryBlock,i8*>"* null, %"struct.AVLNode<MemoryBlock,i8*>"** %39, align 4
  %40 = getelementptr inbounds i8* %23, i32 20
  %41 = bitcast i8* %40 to %"struct.AVLNode<MemoryBlock,i8*>"**
  store %"struct.AVLNode<MemoryBlock,i8*>"* null, %"struct.AVLNode<MemoryBlock,i8*>"** %41, align 4
  %42 = getelementptr inbounds i8* %23, i32 24
  %43 = bitcast i8* %42 to i32*
  store i32 0, i32* %43, align 4
  %44 = icmp eq i8* %23, null
  br i1 %44, label %bb9, label %bb10

bb9:                                              ; preds = %bb7
  %45 = call %"struct.std::ostream"* @_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc(%"struct.std::ostream"* @_ZSt4cerr, i8* getelementptr inbounds ([31 x i8]* @.str14, i32 0, i32 0))
  %46 = call %"struct.std::ostream"* @_ZNSolsEPFRSoS_E(%"struct.std::ostream"* %45, %"struct.std::ostream"* (%"struct.std::ostream"*)* @_ZSt4endlIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_)
  call void @exit(i32 -1) noreturn nounwind
  unreachable

bb10:                                             ; preds = %bb7
  %47 = load %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  %48 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %47, null
  br i1 %48, label %bb11, label %bb12

bb11:                                             ; preds = %bb10
  store %"struct.AVLNode<MemoryBlock,i8*>"* %24, %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  br label %return

bb12:                                             ; preds = %bb10
  store %"struct.AVLNode<MemoryBlock,i8*>"* %47, %"struct.AVLNode<MemoryBlock,i8*>"** %41, align 4
  %49 = load %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  %50 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %49, i32 0, i32 0, i32 0
  %51 = load i8** %50, align 4
  %52 = icmp ult i8* %26, %51
  br i1 %52, label %bb14, label %bb1.i27

bb1.i27:                                          ; preds = %bb12
  %53 = icmp eq i8* %26, %51
  br i1 %53, label %bb2.i28, label %bb15

bb2.i28:                                          ; preds = %bb1.i27
  %54 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %49, i32 0, i32 0, i32 1
  %55 = load i32* %54, align 4
  %56 = icmp slt i32 %29, %55
  br i1 %56, label %bb14, label %bb15

bb14:                                             ; preds = %bb2.i28, %bb12
  %57 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %49, i32 0, i32 1
  store %"struct.AVLNode<MemoryBlock,i8*>"* %24, %"struct.AVLNode<MemoryBlock,i8*>"** %57, align 4
  br label %bb33

bb15:                                             ; preds = %bb2.i28, %bb1.i27
  %58 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %49, i32 0, i32 2
  store %"struct.AVLNode<MemoryBlock,i8*>"* %24, %"struct.AVLNode<MemoryBlock,i8*>"** %58, align 4
  br label %bb33

bb17:                                             ; preds = %bb33
  store %"struct.AVLNode<MemoryBlock,i8*>"* %102, %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  %59 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %102, i32 0, i32 1
  %60 = load %"struct.AVLNode<MemoryBlock,i8*>"** %59, align 4
  %61 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %60, %p.1
  %62 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %102, i32 0, i32 4
  %63 = load i32* %62, align 4
  %64 = add nsw i32 %63, 1
  %65 = add nsw i32 %63, -1
  %storemerge = select i1 %61, i32 %65, i32 %64
  store i32 %storemerge, i32* %62, align 4
  %66 = load %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  %67 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %66, i32 0, i32 4
  %68 = load i32* %67, align 4
  switch i32 %68, label %bb24 [
    i32 0, label %bb34
    i32 1, label %bb33
    i32 -1, label %bb33
  ]

bb24:                                             ; preds = %bb17
  %69 = icmp slt i32 %68, 0
  %70 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.1, i32 0, i32 4
  %71 = load i32* %70, align 4
  br i1 %69, label %bb25, label %bb29

bb25:                                             ; preds = %bb24
  %72 = icmp slt i32 %71, 0
  br i1 %72, label %bb26, label %bb27

bb26:                                             ; preds = %bb25
  %73 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %66, i32 0, i32 1
  %74 = load %"struct.AVLNode<MemoryBlock,i8*>"** %73, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %74, %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  %75 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %74, i32 0, i32 2
  %76 = load %"struct.AVLNode<MemoryBlock,i8*>"** %75, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %76, %"struct.AVLNode<MemoryBlock,i8*>"** %73, align 4
  %77 = load %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  %78 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %77, i32 0, i32 2
  store %"struct.AVLNode<MemoryBlock,i8*>"* %66, %"struct.AVLNode<MemoryBlock,i8*>"** %78, align 4
  %79 = load %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  %80 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %66, i32 0, i32 3
  %81 = load %"struct.AVLNode<MemoryBlock,i8*>"** %80, align 4
  %82 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %79, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %81, %"struct.AVLNode<MemoryBlock,i8*>"** %82, align 4
  %83 = load %"struct.AVLNode<MemoryBlock,i8*>"** %73, align 4
  %84 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %83, null
  br i1 %84, label %_ZN7AVLTreeI11MemoryBlockPcE7rotateRERP7AVLNodeIS0_S1_E.exit, label %bb.i21

bb.i21:                                           ; preds = %bb26
  %85 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %83, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %66, %"struct.AVLNode<MemoryBlock,i8*>"** %85, align 4
  br label %_ZN7AVLTreeI11MemoryBlockPcE7rotateRERP7AVLNodeIS0_S1_E.exit

_ZN7AVLTreeI11MemoryBlockPcE7rotateRERP7AVLNodeIS0_S1_E.exit: ; preds = %bb.i21, %bb26
  %86 = load %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %86, %"struct.AVLNode<MemoryBlock,i8*>"** %80, align 4
  %87 = load %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  store i32 0, i32* %67, align 4
  %88 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %87, i32 0, i32 4
  store i32 0, i32* %88, align 4
  br label %bb34

bb27:                                             ; preds = %bb25
  call void @_ZN7AVLTreeI11MemoryBlockPcE8rotateLRERP7AVLNodeIS0_S1_E(%"struct.AVLTree<MemoryBlock,i8*>"* %this, %"struct.AVLNode<MemoryBlock,i8*>"** %parent) nounwind
  br label %bb34

bb29:                                             ; preds = %bb24
  %89 = icmp sgt i32 %71, 0
  br i1 %89, label %bb30, label %bb31

bb30:                                             ; preds = %bb29
  %90 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %66, i32 0, i32 2
  %91 = load %"struct.AVLNode<MemoryBlock,i8*>"** %90, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %91, %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  %92 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %91, i32 0, i32 1
  %93 = load %"struct.AVLNode<MemoryBlock,i8*>"** %92, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %93, %"struct.AVLNode<MemoryBlock,i8*>"** %90, align 4
  store %"struct.AVLNode<MemoryBlock,i8*>"* %66, %"struct.AVLNode<MemoryBlock,i8*>"** %92, align 4
  %94 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %66, i32 0, i32 3
  %95 = load %"struct.AVLNode<MemoryBlock,i8*>"** %94, align 4
  %96 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %91, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %95, %"struct.AVLNode<MemoryBlock,i8*>"** %96, align 4
  %97 = load %"struct.AVLNode<MemoryBlock,i8*>"** %90, align 4
  %98 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %97, null
  br i1 %98, label %_ZN7AVLTreeI11MemoryBlockPcE7rotateLERP7AVLNodeIS0_S1_E.exit, label %bb.i17

bb.i17:                                           ; preds = %bb30
  %99 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %97, i32 0, i32 3
  store %"struct.AVLNode<MemoryBlock,i8*>"* %66, %"struct.AVLNode<MemoryBlock,i8*>"** %99, align 4
  br label %_ZN7AVLTreeI11MemoryBlockPcE7rotateLERP7AVLNodeIS0_S1_E.exit

_ZN7AVLTreeI11MemoryBlockPcE7rotateLERP7AVLNodeIS0_S1_E.exit: ; preds = %bb.i17, %bb30
  store %"struct.AVLNode<MemoryBlock,i8*>"* %91, %"struct.AVLNode<MemoryBlock,i8*>"** %94, align 4
  store i32 0, i32* %67, align 4
  %100 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %91, i32 0, i32 4
  store i32 0, i32* %100, align 4
  br label %bb34

bb31:                                             ; preds = %bb29
  call void @_ZN7AVLTreeI11MemoryBlockPcE8rotateRLERP7AVLNodeIS0_S1_E(%"struct.AVLTree<MemoryBlock,i8*>"* %this, %"struct.AVLNode<MemoryBlock,i8*>"** %parent) nounwind
  br label %bb34

bb33:                                             ; preds = %bb17, %bb17, %bb15, %bb14
  %p.1 = phi %"struct.AVLNode<MemoryBlock,i8*>"* [ %24, %bb15 ], [ %24, %bb14 ], [ %66, %bb17 ], [ %66, %bb17 ]
  %101 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %p.1, i32 0, i32 3
  %102 = load %"struct.AVLNode<MemoryBlock,i8*>"** %101, align 4
  %103 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %102, null
  br i1 %103, label %bb34, label %bb17

bb34:                                             ; preds = %bb33, %bb31, %_ZN7AVLTreeI11MemoryBlockPcE7rotateLERP7AVLNodeIS0_S1_E.exit, %bb27, %_ZN7AVLTreeI11MemoryBlockPcE7rotateRERP7AVLNodeIS0_S1_E.exit, %bb17
  %104 = load %"struct.AVLNode<MemoryBlock,i8*>"** %parent, align 4
  %105 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %104, i32 0, i32 3
  %106 = load %"struct.AVLNode<MemoryBlock,i8*>"** %105, align 4
  %107 = icmp eq %"struct.AVLNode<MemoryBlock,i8*>"* %106, null
  br i1 %107, label %bb35, label %bb36

bb35:                                             ; preds = %bb34
  store %"struct.AVLNode<MemoryBlock,i8*>"* %104, %"struct.AVLNode<MemoryBlock,i8*>"** %ptr, align 4
  br label %return

bb36:                                             ; preds = %bb34
  %108 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %104, i32 0, i32 0, i32 0
  %109 = load i8** %108, align 4
  %110 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %106, i32 0, i32 0, i32 0
  %111 = load i8** %110, align 4
  %112 = icmp ult i8* %109, %111
  br i1 %112, label %bb38, label %bb1.i5

bb1.i5:                                           ; preds = %bb36
  %113 = icmp eq i8* %109, %111
  br i1 %113, label %bb2.i6, label %bb39

bb2.i6:                                           ; preds = %bb1.i5
  %114 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %104, i32 0, i32 0, i32 1
  %115 = load i32* %114, align 4
  %116 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %106, i32 0, i32 0, i32 1
  %117 = load i32* %116, align 4
  %118 = icmp slt i32 %115, %117
  br i1 %118, label %bb38, label %bb39

bb38:                                             ; preds = %bb2.i6, %bb36
  %119 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %106, i32 0, i32 1
  store %"struct.AVLNode<MemoryBlock,i8*>"* %104, %"struct.AVLNode<MemoryBlock,i8*>"** %119, align 4
  br label %return

bb39:                                             ; preds = %bb2.i6, %bb1.i5
  %120 = getelementptr inbounds %"struct.AVLNode<MemoryBlock,i8*>"* %106, i32 0, i32 2
  store %"struct.AVLNode<MemoryBlock,i8*>"* %104, %"struct.AVLNode<MemoryBlock,i8*>"** %120, align 4
  br label %return

return:                                           ; preds = %bb39, %bb38, %bb35, %bb11, %bb.i
  %.035 = phi i8 [ 1, %bb11 ], [ 0, %bb.i ], [ 1, %bb39 ], [ 1, %bb38 ], [ 1, %bb35 ]
  ret i8 %.035
}

declare i8* @_Znwj(i32)

declare void @exit(i32) noreturn nounwind

declare extern_weak i32 @pthread_once(i32*, void ()*)

declare extern_weak i8* @pthread_getspecific(i32)

declare extern_weak i32 @pthread_setspecific(i32, i8*)

declare extern_weak i32 @pthread_create(i32*, %union.pthread_attr_t*, i8* (i8*)*, i8*)

declare extern_weak i32 @pthread_cancel(i32)

declare extern_weak i32 @pthread_mutex_lock(%union.pthread_mutex_t*)

declare extern_weak i32 @pthread_mutex_trylock(%union.pthread_mutex_t*)

declare extern_weak i32 @pthread_mutex_unlock(%union.pthread_mutex_t*)

declare extern_weak i32 @pthread_mutex_init(%union.pthread_mutex_t*, %union..0._10*)

declare extern_weak i32 @pthread_key_create(i32*, void (i8*)*)

declare extern_weak i32 @pthread_key_delete(i32)

declare extern_weak i32 @pthread_mutexattr_init(%union..0._10*)

declare extern_weak i32 @pthread_mutexattr_settype(%union..0._10*, i32)

declare extern_weak i32 @pthread_mutexattr_destroy(%union..0._10*)
